﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorizationServer.API.ViewModels
{
    public class VMLoginUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool RememberMe { get; set; }
    }
}