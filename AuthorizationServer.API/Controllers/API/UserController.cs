﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AuthorizationServer.API.Controllers.API
{
    public class UserController : ApiController
    {
        ApplicationUserManager manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [Authorize]
        [HttpGet]
        public async Task<IHttpActionResult> Me()
        {
            var currentUser = manager.FindById(User.Identity.GetUserId());
            return Ok(currentUser.getApiUser());
        }
    }
}