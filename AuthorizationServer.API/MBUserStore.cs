﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Diagnostics;
using System.Linq;
using AuthorizationServer.API.Models;

namespace AuthorizationServer.API
{
    public class MBUserStore : IUserStore<ApplicationUser>, 
        IUserPasswordStore<ApplicationUser>,
        IUserLockoutStore<ApplicationUser,string>,
        IUserTwoFactorStore<ApplicationUser, string>
    {
        private Entities db = new Entities();

        public MBUserStore()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }


        public Task CreateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            db.Dispose();
        }

        public async Task<ApplicationUser> FindByIdAsync(int userId) {
            tWebUser webUser = await this.db.tWebUsers
                .Include(u => u.tPersonal_Information)
                .Include(u => u.tWebUser_Role.Select(y=> y.tRole))
                .SingleOrDefaultAsync(p => p.id == userId);
            ApplicationUser user = new ApplicationUser(webUser);
            return user;
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId)
        {
            int intUserId = Convert.ToInt32(userId);
            return await this.FindByIdAsync(intUserId);
        }

        public async Task<ApplicationUser> FindByNameAsync(string userName)
        {
            tPersonal_Information person = await this.db.tPersonal_Information
                .Include(p => p.tWebUser)
                .Where(p => (p.Person_No == userName || p.Email == userName) && p.tWebUser.activated == true && p.tWebUser.active == true)
                .FirstOrDefaultAsync();

            if (person ==null) return null;

            ApplicationUser user = new ApplicationUser(person);
            return user;
        }

        public Task<int> GetAccessFailedCountAsync(ApplicationUser user)
        {
            return Task.FromResult(0);
        }

        public Task<bool> GetLockoutEnabledAsync(ApplicationUser user)
        {
            return Task.FromResult(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(ApplicationUser user)
        {
            return Task.FromResult(new DateTimeOffset());
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> GetTwoFactorEnabledAsync(ApplicationUser user)
        {
            return Task.FromResult(false);
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(ApplicationUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public Task SetTwoFactorEnabledAsync(ApplicationUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
    }
    
}