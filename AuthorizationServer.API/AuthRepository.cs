﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuthorizationServer.API.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace AuthorizationServer.API
{
    public class AuthRepository : IDisposable
    {
        private ApplicationUserManager _userManager;

        public AuthRepository()
        {
            _userManager = new ApplicationUserManager(new MBUserStore());
        }

        public async Task<IdentityResult> RegisterUser(ApplicationUser user)
        {
            var result = await _userManager.CreateAsync(user, user.Password);

            return result;
        }

        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public async Task<System.Security.Claims.ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUser user, string authType) {
            var userIdentity = await _userManager.CreateIdentityAsync(user, authType);
            return userIdentity;
        }

        public void Dispose()
        {
            _userManager.Dispose();

        }
    }
}