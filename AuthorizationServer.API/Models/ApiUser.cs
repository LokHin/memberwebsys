﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthorizationServer.API.Models
{
    public class ApiUser
    {
        //User Object for expose to public service
        public String id { get; set; }
        public String UserName { get; set; }
        public String Email { get; set; }
        public String DisplayName { get; set; }
        public List<string> Roles{ get; set; }

        public ApiUser(ApplicationUser appUser) {
            this.id = appUser.Id;
            this.UserName = appUser.UserName;
            this.Email = appUser.Email;
            this.DisplayName = appUser.DisplayName;
            this.Roles = appUser.webUser.tWebUser_Role.Select(x => x.tRole.name).ToList<string>();
        }
    }
}