//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuthorizationServer.API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tWebUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tWebUser()
        {
            this.tWebUser_Role = new HashSet<tWebUser_Role>();
            this.tPersonal_Information = new HashSet<tPersonal_Information>();
        }
    
        public int id { get; set; }
        public string password { get; set; }
        public bool active { get; set; }
        public string single_access_token { get; set; }
        public bool activated { get; set; }
        public Nullable<System.DateTimeOffset> last_login { get; set; }
        public Nullable<System.DateTimeOffset> created_at { get; set; }
        public Nullable<System.DateTimeOffset> updated_at { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tWebUser_Role> tWebUser_Role { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tPersonal_Information> tPersonal_Information { get; set; }
    }
}
