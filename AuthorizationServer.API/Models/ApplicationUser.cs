﻿using System;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace AuthorizationServer.API.Models
{
    public class ApplicationUser : IUser<string>
    {
        public tWebUser webUser { get; }
        public tPersonal_Information associatedPerson{
            get {
                IEnumerator<tPersonal_Information> e = webUser.tPersonal_Information.GetEnumerator();
                tPersonal_Information person = null;
                while (e.MoveNext()) {
                    person = e.Current;
                }

                return person;
            }
        }
        public string Id
        {
            get { return Convert.ToString(webUser.id); }
        }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public bool RememberMe { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string PasswordHash
        {
            get { return webUser.password; }
            set { webUser.password = value; }
        }
        public DateTimeOffset? LastLogin {
            get { return webUser.last_login; }
            set { webUser.last_login = value; }
        }
        public string PhoneNumber { get; set; }

        public bool hasRole(string roleName) {
            int count = this.webUser.tWebUser_Role.Where(r => r.tRole.name == roleName && r.tRole.tApplication.name == "MG12").Count();
            return count>0 ? true: false;
        }

        public ApplicationUser() { }

        public ApplicationUser(tWebUser webUser)
        {
            this.webUser = webUser;
            this.UserName = associatedPerson.Person_No;
            this.Email = associatedPerson.Email;
            this.DisplayName = associatedPerson.Display_Name;
        }

        public ApplicationUser(tPersonal_Information person) : this(person.tWebUser)
        {

        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here

            


            return userIdentity;
        }

        public ApiUser getApiUser() {
            return new ApiUser(this);
        }
    }
}