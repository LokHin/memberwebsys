﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Infrastructure;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.Owin.Logging;
using System.Net.Http;

namespace Church611OwinAuth
{
    class Church611AuthenticationMiddleware : AuthenticationMiddleware<Church611AuthenticationOptions>
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public Church611AuthenticationMiddleware(OwinMiddleware next, IAppBuilder app, Church611AuthenticationOptions options) : base(next, options) {
            


            _logger = app.CreateLogger<Church611AuthenticationMiddleware>();

            if (options.StateDataFormat == null)
            {
                var dataProtector = app.CreateDataProtector(typeof(Church611AuthenticationMiddleware).FullName,
                    options.AuthenticationType);

                options.StateDataFormat = new PropertiesDataFormat(dataProtector);
            }

            if (Options.Provider == null)
                Options.Provider = new Church611AuthenticationProvider();

            if (string.IsNullOrEmpty(Options.SignInAsAuthenticationType))
            {
                options.SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType();
            }


            _httpClient = new HttpClient();
        }

        protected override AuthenticationHandler<Church611AuthenticationOptions> CreateHandler() {
           return new Church611AuthenticationHandler(_httpClient, _logger);
        }
        
    }
}
