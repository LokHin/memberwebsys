﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Church611OwinAuth
{
    public static class Church611AuthenticationExtensions
    {
        public static IAppBuilder UseChurch611Authentication(this IAppBuilder app, Church611AuthenticationOptions options) {

            if (app == null)
                throw new ArgumentNullException(nameof(app));
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            app.Use(typeof(Church611AuthenticationMiddleware), app, options);

            return app;
        }

        public static IAppBuilder UseChurch611Authentication(this IAppBuilder app, string clientId, string secretKey)
        {

            return app.UseChurch611Authentication(new Church611AuthenticationOptions(clientId, secretKey));
        }

    }
}
