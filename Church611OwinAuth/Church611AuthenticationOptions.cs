﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Church611OwinAuth
{
    public class Church611AuthenticationOptions : AuthenticationOptions
    {
        
        public string AuthServerUrl { get; set; }
        public string AuthorizationEndpoint { get; set; }
        public string TokenEndpoint { get; set; }
        public string UserEndpoint { get; set; }
        public PathString CallbackPath { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string SignInAsAuthenticationType { get; set; }
        public IChurch611AuthenticationProvider Provider { get; set; }
        public ISecureDataFormat<AuthenticationProperties> StateDataFormat { get; set; }


        public Church611AuthenticationOptions(string clientId, string clientSecret) : base(Constants.DefaultAuthenticationType)
        {
            Description.Caption = Constants.DefaultAuthenticationType;
            CallbackPath = new PathString("/signin-church611");
            AuthenticationMode = AuthenticationMode.Passive;
            ClientId = clientId;
            ClientSecret = clientSecret;
            AuthServerUrl = Constants.AuthServerUrl;
            AuthorizationEndpoint = Constants.AuthorizationEndpoint;
            TokenEndpoint = Constants.TokenEndpoint;
            UserEndpoint = Constants.UserEndpoint;
        }
    }
}
