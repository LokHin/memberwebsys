﻿using System.Threading.Tasks;

namespace Church611OwinAuth
{
    public interface IChurch611AuthenticationProvider
    {
        Task Authenticated(Church611AuthenticatedContext context);
        Task ReturnEndpoint(Church611ReturnEndpointContext context);
    }
}
