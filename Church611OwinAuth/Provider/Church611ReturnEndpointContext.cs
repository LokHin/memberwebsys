﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Provider;

namespace Church611OwinAuth
{
    public class Church611ReturnEndpointContext : ReturnEndpointContext
    {
        public Church611ReturnEndpointContext(IOwinContext context, AuthenticationTicket ticket) : base(context, ticket)
        {

        }
    }
}
