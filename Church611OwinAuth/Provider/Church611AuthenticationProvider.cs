﻿using System;
using System.Threading.Tasks;

namespace Church611OwinAuth
{
    public class Church611AuthenticationProvider : IChurch611AuthenticationProvider
    {
        public Church611AuthenticationProvider() {
            OnAuthenticated = context => Task.FromResult<object>(null);
            OnReturnEndpoint = context => Task.FromResult<object>(null);
        }

        public Func<Church611AuthenticatedContext, Task> OnAuthenticated { get; set; }
        public Func<Church611ReturnEndpointContext, Task> OnReturnEndpoint { get; set; }

        public virtual Task Authenticated(Church611AuthenticatedContext context)
        {
            return OnAuthenticated(context);
        }

        public virtual Task ReturnEndpoint(Church611ReturnEndpointContext context)
        {
            return OnReturnEndpoint(context);
        }
    }
}
