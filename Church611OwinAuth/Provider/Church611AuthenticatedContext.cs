﻿using System.Security.Claims;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Provider;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Dynamic;

namespace Church611OwinAuth
{
    public class Church611AuthenticatedContext : BaseContext
    {
        public Church611AuthenticatedContext(IOwinContext context, ExpandoObject user, string accessToken) : base(context)
        {
            User = user;
            AccessToken = accessToken;

            Id = User.userName;
            DisplayName = User.displayName; 
            Email = User.email;
            Roles = new List<string>();

            foreach (var role in User.roles) {
                Roles.Add(role);
            }
        }

        public dynamic User { get; private set; }
        public string AccessToken { get; private set; }

        public string Id { get; private set; }
        public string DisplayName { get; private set; }
        public string Email { get; private set; }
        public List<string> Roles { get; private set; }

        public ClaimsIdentity Identity { get; set; }
        public AuthenticationProperties Properties { get; set; }

        private static string TryGetValue(JObject user, string propertyName)
        {
            JToken value;
            return user.TryGetValue(propertyName, out value) ? value.ToString() : null;
        }
    }
}
