﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Church611OwinAuth
{
    internal static class Constants
    {
        internal const string DefaultAuthenticationType = "Church611";
        internal const string AuthServerUrl = "http://localhost:51360";
        //internal const string AuthServerUrl = "https://auth.church611.org";
        internal const string AuthorizationEndpoint = "/oauth/authorize";
        internal const string TokenEndpoint = "/oauth/token";
        internal const string UserEndpoint = "/api/user";
    }
}
