﻿using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Logging;
using System.Net.Http;
using Microsoft.Owin;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Dynamic;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace Church611OwinAuth
{
    class Church611AuthenticationHandler : AuthenticationHandler<Church611AuthenticationOptions>
    {
        private const string XmlSchemaString = "http://www.w3.org/2001/XMLSchema#string";
        private readonly ILogger _logger;
        private readonly HttpClient _httpClient;

        public Church611AuthenticationHandler(HttpClient httpClient, ILogger logger) {
            _logger = logger;
            _httpClient = httpClient;
        }
        


        protected override async Task<AuthenticationTicket> AuthenticateCoreAsync() {
            AuthenticationProperties props = null;

            try
            {
                string code = null;
                string state = null;

                var query = Request.Query;
                var values = query.GetValues("code");
                if (values != null && values.Count == 1)
                {
                    code = values[0];
                }

                values = query.GetValues("state");
                if (values != null && values.Count == 1)
                {
                    state = values[0];
                }

                props = Options.StateDataFormat.Unprotect(state);

                if (props == null)
                {
                    return null;
                }

                
                // Anti-CSRF
                
                if (!ValidateCorrelationId(props, _logger))
                {
                    return new AuthenticationTicket(null, props);
                }
                
                

                string requestPrefix = Request.Scheme + "://" + Request.Host;
                string redirectUri = requestPrefix + Request.PathBase + Options.CallbackPath;

                // Build up the body for the token request
                var body = new List<KeyValuePair<string, string>>();
                body.Add(new KeyValuePair<string, string>("grant_type", "authorization_code"));
                body.Add(new KeyValuePair<string, string>("code", code));
                body.Add(new KeyValuePair<string, string>("redirect_uri", redirectUri));
                body.Add(new KeyValuePair<string, string>("client_id", Options.ClientId));
                body.Add(new KeyValuePair<string, string>("client_secret", Options.ClientSecret));

                // Request the token
                var TokenEndpointPath = string.Format("{0}{1}", Options.AuthServerUrl, Options.TokenEndpoint);
                HttpResponseMessage tokenResponse = await _httpClient.PostAsync(TokenEndpointPath, new FormUrlEncodedContent(body));
                tokenResponse.EnsureSuccessStatusCode();
                string text = await tokenResponse.Content.ReadAsStringAsync();

                // Deserialize the token response
                dynamic response = JsonConvert.DeserializeObject<dynamic>(text);
                string accessToken = (string)response.access_token;
                string expires = (string)response.expires_in;
                string refreshToken = null;
                if (response.refresh_token != null)
                    refreshToken = (string)response.refresh_token;

                // Get the User info using the bearer token
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(string.Format("{0}{1}", Options.AuthServerUrl, Options.UserEndpoint)),
                    Method = HttpMethod.Get
                };
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage userResponse = await _httpClient.SendAsync(request);
                userResponse.EnsureSuccessStatusCode();

                text = await userResponse.Content.ReadAsStringAsync();
                
                var converter = new ExpandoObjectConverter();
                dynamic user = JsonConvert.DeserializeObject<ExpandoObject>(text, converter);

                //var user = JObject.Parse(text);

                var context = new Church611AuthenticatedContext(Context, user, accessToken) {
                    Identity = new ClaimsIdentity(Options.AuthenticationType, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType)
                };

                if (!string.IsNullOrEmpty(context.Id)) {
                    context.Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, context.Id, XmlSchemaString, Options.AuthenticationType));
                }

                if (!string.IsNullOrEmpty(context.DisplayName))
                {
                    context.Identity.AddClaim(new Claim(ClaimTypes.Name, context.DisplayName, XmlSchemaString, Options.AuthenticationType));
                }

                if (!string.IsNullOrEmpty(context.Email))
                {
                    context.Identity.AddClaim(new Claim(ClaimTypes.Email, context.Email, XmlSchemaString, Options.AuthenticationType));
                }
                
                foreach (var role in context.Roles)
                {
                    context.Identity.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
                }

                // Create a delegate to filter out non-name claims
                //Func<dynamic, bool> matchClaimType = x => x.type != ClaimsIdentity.DefaultNameClaimType;

                /*
                // Add the other claims minus the name since we already added that
                foreach (var claim in System.Linq.Enumerable.Where<dynamic>(user.claims, matchClaimType))
                {
                    identity.AddClaim(new Claim(claim.type, claim.value));
                }
                */
                context.Properties = props;

                await Options.Provider.Authenticated(context);

                return new AuthenticationTicket(context.Identity, context.Properties);
            }
            catch (Exception ex)
            {
                _logger.WriteError(ex.Message);
            }

            return new AuthenticationTicket(null, props);
            /*
            var identity = new ClaimsIdentity(Options.SignInAsAuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, Options.ClientId, null, Options.AuthenticationType));

            var properties = Options.StateDataFormat.Unprotect(Request.Query["state"]);

            return Task.FromResult(new AuthenticationTicket(identity, properties));
            */
        }

        protected override Task ApplyResponseChallengeAsync() {
            if (Response.StatusCode != 401)
            {
                return Task.FromResult<object>(null);
            }

            var challenge = Helper.LookupChallenge(Options.AuthenticationType, Options.AuthenticationMode);

            if (challenge == null) return Task.FromResult<object>(null);
            var baseUri =
                Request.Scheme +
                Uri.SchemeDelimiter +
                Request.Host +
                Request.PathBase;

            var currentUri =
                baseUri +
                Request.Path +
                Request.QueryString;

            var redirectUri =
                baseUri +
                Options.CallbackPath;

            var properties = challenge.Properties;
            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                properties.RedirectUri = currentUri;
            }

            // OAuth2 10.12 CSRF
            GenerateCorrelationId(properties);

            var state = Options.StateDataFormat.Protect(properties);
            var authorizationEndpoint =
                string.Format("{0}{1}", Options.AuthServerUrl, Options.AuthorizationEndpoint) +
                "?response_type=code" +
                "&client_id=" + Uri.EscapeDataString(Options.ClientId) +
                "&redirect_uri=" + Uri.EscapeDataString(redirectUri) +
                "&state=" + Uri.EscapeDataString(state)
                ;

            Response.Redirect(authorizationEndpoint);

            return Task.FromResult<object>(null);
        }

        public override async Task<bool> InvokeAsync()
        {
            // This is always invoked on each request. For passive middleware, only do anything if this is
            // for our callback path when the user is redirected back from the authentication provider.

            if (!Options.CallbackPath.HasValue || Options.CallbackPath != Request.Path) return false;
            
            var ticket = await AuthenticateAsync();

            if (ticket == null) {
                _logger.WriteWarning("Invalid return state, unable to redirect.");
                Response.StatusCode = 500;
                return true;
            }
            
            var context = new Church611ReturnEndpointContext(Context, ticket)
            {
                SignInAsAuthenticationType = Options.SignInAsAuthenticationType,
                RedirectUri = ticket.Properties.RedirectUri
            };

            await Options.Provider.ReturnEndpoint(context);



            if (context.SignInAsAuthenticationType != null && context.Identity != null) {
                var grantIdentity = context.Identity;
                if (!string.Equals(grantIdentity.AuthenticationType, context.SignInAsAuthenticationType, StringComparison.Ordinal)) {
                    grantIdentity = new ClaimsIdentity(grantIdentity.Claims, context.SignInAsAuthenticationType, grantIdentity.NameClaimType, grantIdentity.RoleClaimType);
                }
                Context.Authentication.SignIn(context.Properties, grantIdentity);
            }
            

            Response.Redirect(ticket.Properties.RedirectUri);
            context.RequestCompleted();
            
            return context.IsRequestCompleted;
        }



    }
}
