﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MemberWebSys.Data;
using MemberWebSys.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.OAuth;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using MemberWebSys.Models;
using Sakura.AspNetCore.Mvc;

namespace MemberWebSys
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            //services.AddMvc()
            //    .AddRazorPagesOptions(options =>
            //    {
            //        options.Conventions.AuthorizeFolder("/Account/Manage");
            //        options.Conventions.AuthorizePage("/Account/Logout");
            //    });

            services.AddMvc();

            var connection = @"data source=10.61.1.21;initial catalog=MBtest;persist security info=True;user id=MBGeneral;password=611MBstaff;Trusted_Connection=True;";
            services.AddDbContext<MBtestContext>(options => options.UseSqlServer(connection));

            services.AddBootstrapPagerGenerator(options =>
            {
                options.ConfigureDefault();
            });

            services.Configure<PagerOptions>(Configuration.GetSection("Pager"));

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = "Church611";
            })
                .AddCookie()
                .AddOAuth("Church611", options =>
                {

                    options.ClientId = "611mg12"; //Configuration["Church611:ClientId"];
                    options.ClientSecret = "9qjWqNA9abMqPoRztSChLJvLSgQZoyGNmr61GvG1"; //Configuration["Church611:ClientSecret"];
                    options.CallbackPath = new PathString("/Auth/Login");

                    string authServerUrl = "http://localhost:51360";

                    options.AuthorizationEndpoint = authServerUrl + "/oauth/authorize";
                    options.TokenEndpoint = authServerUrl + "/oauth/token";
                    options.UserInformationEndpoint = authServerUrl + "/api/user";

                    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
                    options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");

                    options.ClaimActions.MapJsonKey("urn:Church611:LoginProvider", "LoginProvider");
                    options.ClaimActions.MapJsonKey("urn:Church611:RedirectUri", "RedirectUri");
                    options.ClaimActions.MapJsonKey("urn:Church611:UserId", "UserId");

                    options.ClaimActions.MapJsonKey("urn:Church611:login", "login");
                    options.ClaimActions.MapJsonKey("urn:Church611:url", "html_url");
                    options.ClaimActions.MapJsonKey("urn:Church611:avatar", "avatar_url");

                    options.Events = new OAuthEvents
                    {
                        OnCreatingTicket = async context =>
                        {
                            var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
                            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);

                            var response = await context.Backchannel.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, context.HttpContext.RequestAborted);
                            response.EnsureSuccessStatusCode();

                            var user = JObject.Parse(await response.Content.ReadAsStringAsync());

                            context.RunClaimActions(user);
                        }
                    };
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
        }
    }
}
