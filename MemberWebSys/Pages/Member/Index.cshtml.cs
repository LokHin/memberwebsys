﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;

namespace MemberWebSys.Pages.Member
{
    public class IndexModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public IndexModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        public IPagedList<TPersonalInformation> TPersonalInformation { get; set; }
        public IList<TMemberCategory> TMemberCategory { get; set; }
        public int mbrCategory { get; set; }
        public string searchStr { get; set; }
        public int pNo { get; set; }

        public async Task<IActionResult> OnGetAsync(int mbrCategory, string searchStr, int pageNo = 1)
        {
            this.mbrCategory = mbrCategory;
            this.searchStr = searchStr;
            int pageSize = 20;

            var items = _context.TPersonalInformation.AsQueryable();

            //Data
            if (this.mbrCategory > 0)
            {
                items = items.Where(o => o.MemberCategory == this.mbrCategory);
            }

            if (this.searchStr != null)
            {
                items = items.Where(o => EF.Functions.Like(o.DisplayName, '%' + this.searchStr + '%') || EF.Functions.Like(o.ChineseName, '%' + this.searchStr + '%') || EF.Functions.Like(o.FirstName, '%' + this.searchStr + '%') || EF.Functions.Like(o.LastName, '%' + this.searchStr + '%') || EF.Functions.Like(o.PersonNo, '%' + this.searchStr + '%') || EF.Functions.Like(o.HomePhone, this.searchStr) || EF.Functions.Like(o.MobilePhone, this.searchStr));
            }

            items = items.OrderBy(o => o.PersonNo);

            TPersonalInformation = await items.ToPagedListAsync(pageSize, pageNo);
            TMemberCategory = await _context.TMemberCategory.ToListAsync();

            pNo = pageNo;

            return Page();

        }
    }
}