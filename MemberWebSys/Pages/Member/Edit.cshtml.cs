﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using MemberWebSys.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace MemberWebSys.Pages.Member
{
    public class EditModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public EditModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [TempData]
        public int PageNo { get; set; }
        [BindProperty]
        public TPersonalInformation TPersonalInformation { get; set; }
        public string imgSrc { get; set; }
        public async Task<IActionResult> OnGetAsync(string id, int pageNo)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (pageNo != 0)
            {
                PageNo = pageNo;
            }

            TPersonalInformation = await _context.TPersonalInformation
                .Include(t => t.MemberCategoryNavigation)
                .Include(t => t.User).SingleOrDefaultAsync(m => m.PersonNo == id);

            if (TPersonalInformation == null)
            {
                return NotFound();
            }

            //Photo Display
            if (TPersonalInformation.Photo != null && TPersonalInformation.Photo.Length > 0)
            {
                var base64 = Convert.ToBase64String(TPersonalInformation.Photo);
                imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            }

            var items = _context.TMemberCategory.AsQueryable();
            ViewData["MemberCategoryList"] = new SelectList((items.Where(o => o.MemberCategoryId != 1)), "MemberCategoryId", "MemberCategory");
            ViewData["Gender"] = new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem {Text = "--請選擇--", Value = "" },
                    new SelectListItem { Text = "男", Value = "M"},
                    new SelectListItem { Text = "女", Value = "F"},
                }, "Value", "Text");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormFile fileSelect, int pageNo)
        {
            if (!ModelState.IsValid)
            {
                PageNo = PageNo;
                return Page();
            }

            //var tPInfo = TPersonalInformation;
            PageNo = pageNo;

            _context.Attach(TPersonalInformation).State = EntityState.Modified;

            //replace with byte formated photo
            if (fileSelect != null && fileSelect.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    fileSelect.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    TPersonalInformation.Photo = fileBytes;
                }
            }
            else
            {
                _context.Entry(TPersonalInformation).Property(t => t.Photo).IsModified = false;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }

            return RedirectToPage("./Index", new { pageNo = PageNo });
        }
    }
}