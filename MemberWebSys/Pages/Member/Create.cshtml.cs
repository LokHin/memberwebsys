﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using MemberWebSys.Models;

namespace MemberWebSys.Pages.Member
{
    public class CreateModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public CreateModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [TempData]
        public int PageNo { get; set; }
        public IActionResult OnGet(int p)
        {
            if (p != 0)
            {
                PageNo = p;
            }
            var items = _context.TMemberCategory.AsQueryable();
            ViewData["MemberCategoryList"] = new SelectList((items.Where(o => o.MemberCategoryId != 1)), "MemberCategoryId", "MemberCategory");
            ViewData["Gender"] = new SelectList(
            new List<SelectListItem>
                {
                    new SelectListItem {Text = "--請選擇--", Value = "" },
                    new SelectListItem { Text = "男", Value = "M"},
                    new SelectListItem { Text = "女", Value = "F"},
                }, "Value", "Text");
            return Page();
        }

        private IActionResult Page(TPersonalInformation TPInfo)
        {
            PageNo = PageNo;
            TPersonalInformation = TPInfo;
            var items = _context.TMemberCategory.AsQueryable();
            ViewData["MemberCategoryList"] = new SelectList((items.Where(o => o.MemberCategoryId != 1)), "MemberCategoryId", "MemberCategory");
            ViewData["Gender"] = new SelectList(
            new List<SelectListItem>
                {
                    new SelectListItem {Text = "--請選擇--", Value = "" },
                    new SelectListItem { Text = "男", Value = "M"},
                    new SelectListItem { Text = "女", Value = "F"},
                }, "Value", "Text");
            return Page();
        }

        [BindProperty]
        public TPersonalInformation TPersonalInformation { get; set; }

        public async Task<IActionResult> OnPostAsync(IFormFile fileSelect, int pageNo)
        {

            if (!ModelState.IsValid)
            {
                return Page(TPersonalInformation);
            }

            if (pageNo != 0)
            {
                PageNo = pageNo;
            }

            //replace with new PersonNo
            using (var sqlConnection = new SqlConnection("data source=10.61.1.21;initial catalog=MBtest;persist security info=True;user id=MBGeneral;password=611MBstaff;Trusted_Connection=True;"))
            {
                using (var cmd = new SqlCommand()
                {
                    CommandText = "SELECT dbo.nextPersonNo() As Person_No",
                    CommandType = CommandType.Text,
                    Connection = sqlConnection
                })
                {
                    sqlConnection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            TPersonalInformation.PersonNo = reader.GetInt32(0).ToString();
                        }
                    }
                }
                sqlConnection.Close();
            }

            //replace with byte formated photo
            if (fileSelect != null && fileSelect.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    fileSelect.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    TPersonalInformation.Photo = fileBytes;
                }
            }

            //Timestamp
            TPersonalInformation.UpdateUser = "Web System";
            TPersonalInformation.LastUpdate = DateTime.Now;

            _context.TPersonalInformation.Add(TPersonalInformation);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index", new { pageNo = PageNo });
        }
    }
}