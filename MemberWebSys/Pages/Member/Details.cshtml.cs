﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.Member
{
    public class DetailsModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public DetailsModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [TempData]
        public int PageNo { get; set; }

        public TPersonalInformation TPersonalInformation { get; set; }

        public string imgSrc { get; set; }

        public async Task<IActionResult> OnGetAsync(string id, int pageNo)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (pageNo != 0)
            {
                PageNo = pageNo;
            }

            TPersonalInformation = await _context.TPersonalInformation
                .Include(t => t.MemberCategoryNavigation)
                .Include(t => t.User).SingleOrDefaultAsync(m => m.PersonNo == id);

            if (TPersonalInformation == null)
            {
                return NotFound();
            }

            //Photo Display
            if (TPersonalInformation.Photo != null && TPersonalInformation.Photo.Length > 0)
            {
                var base64 = Convert.ToBase64String(TPersonalInformation.Photo);
                imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            }

            return Page();
        }
    }
}