﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace MemberWebSys.Pages
{
    public class IndexModel : PageModel
    {
        public string Church611Avatar { get; set; }

        public string Church611Login { get; set; }

        public string Church611Name { get; set; }

        public string Church611Url { get; set; }

        public void OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                Church611Name = User.FindFirst(c => c.Type == ClaimTypes.Name)?.Value;
                Church611Login = User.FindFirst(c => c.Type == "urn:Church611:login")?.Value;
                Church611Url = User.FindFirst(c => c.Type == "urn:Church611:url")?.Value;
                Church611Avatar = User.FindFirst(c => c.Type == "urn:Church611:avatar")?.Value;
            }
        }
    }
}
