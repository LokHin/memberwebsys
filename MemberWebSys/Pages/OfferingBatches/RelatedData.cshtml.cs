﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;


namespace MemberWebSys.Pages.OfferingBatches
{
    public class RelatedDataModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;
        public RelatedDataModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        private List<JsonPersonInfo> GetPersonData(string value, string key)
        {
            if (key == "offeringName")
            {
                var personQuery = _context.TPersonalInformation.AsQueryable().Where(o => o.OfferingName == value).ToList();
                List<JsonPersonInfo> JsonPersonList = new List<JsonPersonInfo>();

                foreach (TPersonalInformation tpi in personQuery)
                {
                    JsonPersonInfo jpi = new JsonPersonInfo();

                    jpi.PersonNo = tpi.PersonNo;
                    jpi.MemberCategory = tpi.MemberCategory;
                    jpi.ChineseName = tpi.ChineseName;
                    jpi.LastName = tpi.LastName;
                    jpi.FirstName = tpi.FirstName;
                    jpi.DisplayName = tpi.DisplayName;
                    jpi.Salutation = tpi.Salutation;
                    jpi.HomePhone = tpi.HomePhone;
                    jpi.WorkPhone = tpi.WorkPhone;
                    jpi.MobilePhone = tpi.MobilePhone;

                    JsonPersonList.Add(jpi);
                }
                return JsonPersonList;
            }
            if (key == "englishName")
            {
                var personQuery = _context.TPersonalInformation.AsQueryable().Where(o => o.FirstName == value).ToList();
                List<JsonPersonInfo> JsonPersonList = new List<JsonPersonInfo>();

                foreach (TPersonalInformation tpi in personQuery)
                {
                    JsonPersonInfo jpi = new JsonPersonInfo();

                    jpi.PersonNo = tpi.PersonNo;
                    jpi.MemberCategory = tpi.MemberCategory;
                    jpi.ChineseName = tpi.ChineseName;
                    jpi.LastName = tpi.LastName;
                    jpi.FirstName = tpi.FirstName;
                    jpi.DisplayName = tpi.DisplayName;
                    jpi.Salutation = tpi.Salutation;
                    jpi.HomePhone = tpi.HomePhone;
                    jpi.WorkPhone = tpi.WorkPhone;
                    jpi.MobilePhone = tpi.MobilePhone;

                    JsonPersonList.Add(jpi);
                }
                return JsonPersonList;
            }
            
            

            return null;
        }

        public JsonResult OnGetPersonInfoList(string offeringName)
        {
            var persons = GetPersonData(offeringName, "offeringName");

            return new JsonResult(persons);
        }

        [TempData]
        public int PageNo { get; set; }
        public DateTime date { get; set; }
        public string time { get; set; }
        public int? inputSeq { get; set; }
        public Boolean create { get; set; }
        public TOfferingBatches TOfferingBatches { get; set; }
        public TPersonalInformation TPersonalInformation { get; set; }
        public IPagedList<TOfferingBatch> TOfferingBatch { get; set; }
        public TOfferingBatch TOfferingBatchS { get; set; }
        public IPagedList<TOfferingItems> TOfferingItems { get; set; }

        public async Task<IActionResult> OnGetAsync(DateTime date, string time, int? inputSeq, Boolean? create, int pageNo = 1)
        {

            var itemsBatch = _context.TOfferingBatch.AsQueryable();
            int pageSize = 10;

            if (pageNo != 0)
            {
                PageNo = pageNo;
            }

            //Data
            TOfferingBatches = await _context.TOfferingBatches
                .SingleOrDefaultAsync(m => m.Date == date && m.Time == time);

            itemsBatch = itemsBatch.Where(o => o.Time == time).AsNoTracking().OrderBy(o => o.Date);

            TOfferingBatch = await itemsBatch
                .Include(t => t.PersonNoNavigation)
                .ToPagedListAsync(pageSize, pageNo);

            this.date = date;
            this.time = time;

            if (create != null)
            {
                this.create = create.Value;
            }

            if (inputSeq != null)
            {
                var itemsItem = _context.TOfferingItems.AsQueryable();
                int pageSizeItem = 10;

                //Data
                itemsItem = itemsItem.Where(o => o.InputSeq == inputSeq).AsNoTracking().OrderBy(o => o.InputSeq);

                TOfferingItems = await itemsItem.Include(t => t.OfferingType).ToPagedListAsync(pageSizeItem, pageNo);

                this.inputSeq = inputSeq.Value;

                TOfferingBatchS = await _context.TOfferingBatch
                .SingleOrDefaultAsync(m => m.InputSeq == inputSeq);
            }

            var batchType = _context.TOfferingType.OrderBy(o => o.Sorting).ToList();

            List<SelectListItem> dlItems = new SelectList(batchType, "OfferingTypeId", "OfferingType").ToList();
            dlItems.Insert(0, (new SelectListItem { Text = "--請選擇--", Value = "" }));
            ViewData["BatchTypeList"] = dlItems;

            return Page();
        }

        private IActionResult Page(TOfferingItems TOItems)
        {
            PageNo = PageNo;
            TOfferingItems_Input = TOItems;
            var batchType = _context.TOfferingType.OrderBy(o => o.Sorting).ToList();
            ViewData["BatchTypeList"] = new SelectList(batchType, "OfferingTypeId", "OfferingType");

            return Page();
        }

        [BindProperty]
        public TOfferingItems TOfferingItems_Input { get; set; }
        [BindProperty]
        public TOfferingBatch TOfferingBatch_Input { get; set; }

        //string time, int inputSeq, 
        public async Task<IActionResult> OnPostAsync(int pageNo = 1)
        {
            //if (!ModelState.IsValid)
            //{
            //    return Page(TOfferingItems_Input);
            //}

            if (pageNo != 0)
            {
                PageNo = pageNo;
            }

            //if (TOfferingItems != null)
            if (TOfferingItems_Input != null)
            {
                //New TOfferingItem

                //Check parent record exist
                var itemsBatch = _context.TOfferingBatch.AsQueryable();
                itemsBatch = itemsBatch.Where(o => o.Date == TOfferingItems_Input.Date && o.Time == TOfferingItems_Input.Time && o.InputSeq == TOfferingItems_Input.InputSeq);
                var TOfferingBatchP = await itemsBatch.ToListAsync();

                if (TOfferingBatchP.Count == 1)
                {
                    _context.TOfferingItems.Add(TOfferingItems_Input);

                    //Sum up TOfferingItem amount. Update TOfferingBatch
                    var itemsitem = _context.TOfferingItems.AsQueryable();
                    decimal total = itemsitem.Where(o => o.Date == TOfferingItems_Input.Date && o.Time == TOfferingItems_Input.Time && o.InputSeq == TOfferingItems_Input.InputSeq).Select(o => o.Amount).DefaultIfEmpty(0).Sum().Value;

                    TOfferingBatch TOfferingBatch = TOfferingBatchP[0];
                    TOfferingBatch.TotalAmt = total + TOfferingItems_Input.Amount;

                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {

                    }
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                //New TOfferingBatch

                _context.TOfferingBatch.Add(TOfferingBatch_Input);
                await _context.SaveChangesAsync();
            }


            return RedirectToPage("./RelatedData", new { date = TOfferingItems_Input.Date, time = TOfferingItems_Input.Time, pageNo = PageNo, inputSeq = TOfferingItems_Input.InputSeq });

        }

    }
}