﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class RelatedBatchEditModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public RelatedBatchEditModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TOfferingBatch TOfferingBatch { get; set; }

        public async Task<IActionResult> OnGetAsync(DateTime date, string time, int inputSeq)
        {
            if (time == null)
            {
                return NotFound();
            }

            TOfferingBatch = await _context.TOfferingBatch.Include(m => m.PersonNoNavigation).SingleOrDefaultAsync(m => m.Date == date && m.Time == time && m.InputSeq == inputSeq);

            if (TOfferingBatch == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(TOfferingBatch).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }

            return RedirectToPage("./Index");
        }

    }
}