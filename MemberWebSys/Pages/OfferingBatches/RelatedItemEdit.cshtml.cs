﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class RelatedItemEditModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public RelatedItemEditModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TOfferingItems TOfferingItems { get; set; }

        public async Task<IActionResult> OnGetAsync(DateTime date, string time, int inputSeq, int offeringTypeId)
        {
            if (time == null)
            {
                return NotFound();
            }

            TOfferingItems = await _context.TOfferingItems.Include(m => m.OfferingType).SingleOrDefaultAsync(m => m.Date == date && m.Time == time && m.InputSeq == inputSeq && m.OfferingTypeId == offeringTypeId);

            if (TOfferingItems == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(TOfferingItems).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }

            return RedirectToPage("./Index");
        }

    }
}