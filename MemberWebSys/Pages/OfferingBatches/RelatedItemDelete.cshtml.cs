﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class RelatedItemDeleteModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public RelatedItemDeleteModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TOfferingItems TOfferingItems { get; set; }

        public async Task<IActionResult> OnGetAsync(DateTime date, String time, int inputSeq, short offeringTypeId)
        {
            if (date == null || time == null || inputSeq == 0 || offeringTypeId == 0)
            {
                return NotFound();
            }

            TOfferingItems = await _context.TOfferingItems.SingleOrDefaultAsync(m => m.Date == date && m.Time == time && m.InputSeq == inputSeq && m.OfferingTypeId == offeringTypeId);

            if (TOfferingItems == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(DateTime date, String time, int inputSeq, short offeringTypeId)
        {
            if (date == null || time == null || inputSeq == 0 || offeringTypeId == 0)
            {
                return NotFound();
            }

            TOfferingItems = await _context.TOfferingItems.FindAsync(date, time, inputSeq, offeringTypeId);

            if (TOfferingItems != null)
            {
                _context.TOfferingItems.Remove(TOfferingItems);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}