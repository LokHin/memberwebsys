﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MemberWebSys.Models;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class DeleteModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public DeleteModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TOfferingBatches TOfferingBatches { get; set; }

        public async Task<IActionResult> OnGetAsync(DateTime? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TOfferingBatches = await _context.TOfferingBatches.SingleOrDefaultAsync(m => m.Date == id);

            if (TOfferingBatches == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(DateTime? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TOfferingBatches = await _context.TOfferingBatches.FindAsync(id);

            if (TOfferingBatches != null)
            {
                _context.TOfferingBatches.Remove(TOfferingBatches);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}