﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MemberWebSys.Models;
using System.Data.SqlClient;
using System.Data;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class CreateModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public CreateModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            //default new BatchNo
            using (var sqlConnection = new SqlConnection("data source=10.61.1.21;initial catalog=MBtest;persist security info=True;user id=MBGeneral;password=611MBstaff;Trusted_Connection=True;"))
            {
                using (var cmd = new SqlCommand()
                {
                    CommandText = "SELECT TOP 1 [Time] FROM[MBtest].[dbo].[tOffering_Batches] ORDER BY[Time] DESC",
                    CommandType = CommandType.Text,
                    Connection = sqlConnection
                })
                {
                    sqlConnection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int tempTime = Convert.ToInt32(reader.GetString(0));
                            autoBatch = (tempTime + 1).ToString();
                        }
                    }
                }
                sqlConnection.Close();
            }


            return Page();
        }

        [BindProperty]
        public TOfferingBatches TOfferingBatches { get; set; }
        public string autoBatch { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //Timestamp
            TOfferingBatches.Posted = "Y";
            TOfferingBatches.LastUpdateBy = "Web System";
            TOfferingBatches.LastUpdateTime = DateTime.Now;
            TOfferingBatches.CreatedBy = "Web System";
            TOfferingBatches.CreatedTime = DateTime.Now;

            _context.TOfferingBatches.Add(TOfferingBatches);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}