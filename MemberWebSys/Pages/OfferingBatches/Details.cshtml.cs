﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MemberWebSys.Models;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class DetailsModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public DetailsModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [TempData]
        public int PageNo { get; set; }

        public TOfferingBatches TOfferingBatches { get; set; }

        public async Task<IActionResult> OnGetAsync(string id, int pageNo)
        {
            if (id == null)
            {
                return NotFound();
            }

            if (pageNo != 0)
            {
                PageNo = pageNo;
            }

            TOfferingBatches = await _context.TOfferingBatches
                .Include(t => t.TOfferingBatch).SingleOrDefaultAsync(m => m.Time == id);

            if (TOfferingBatches == null)
            {
                return NotFound();
            }

            return Page();
        }
    }
}