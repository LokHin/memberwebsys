﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class EditModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public EditModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TOfferingBatches TOfferingBatches { get; set; }

        public async Task<IActionResult> OnGetAsync(string time)
        {
            if (time == null)
            {
                return NotFound();
            }

            TOfferingBatches = await _context.TOfferingBatches.Include(m => m.TOfferingBatch).SingleOrDefaultAsync(m => m.Time == time);

            if (TOfferingBatches == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(TOfferingBatches).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }

            return RedirectToPage("./Index");
        }
    }
}