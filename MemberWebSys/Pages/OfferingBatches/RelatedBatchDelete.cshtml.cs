﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class RelatedBatchDeleteModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public RelatedBatchDeleteModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TOfferingBatch TOfferingBatch { get; set; }
        public String err { get; set; }

        public async Task<IActionResult> OnGetAsync(DateTime date, String time, int inputSeq, String err)
        {
            if ((date == null || time == null || inputSeq == 0) && err != null)
            {
                return NotFound();
            }

            this.err = err;

            TOfferingBatch = await _context.TOfferingBatch.SingleOrDefaultAsync(m => m.Date == date && m.Time == time && m.InputSeq == inputSeq);

            if (TOfferingBatch == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(DateTime date, String time, int inputSeq, String err)
        {
            if (date == null || time == null || inputSeq == 0)
            {
                return NotFound();
            }

            //Check child exist
            var TOfferingItems = _context.TOfferingItems.AsQueryable();
            TOfferingItems = _context.TOfferingItems.Where(m => m.Date == date && m.Time == time && m.InputSeq == inputSeq);
            if (TOfferingItems != null)
            {
                return RedirectToPage("./RelatedBatchDelete", new { date = TOfferingBatch.Date, time = TOfferingBatch.Time, inputSeq = TOfferingBatch.InputSeq, err = "child" });
            }

            TOfferingBatch = await _context.TOfferingBatch.FindAsync(date, time, inputSeq);

            if (TOfferingBatch != null)
            {
                _context.TOfferingBatch.Remove(TOfferingBatch);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}