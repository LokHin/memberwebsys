﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemberWebSys.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sakura.AspNetCore;

namespace MemberWebSys.Pages.OfferingBatches
{
    public class IndexModel : PageModel
    {
        private readonly MemberWebSys.Models.MBtestContext _context;

        public IndexModel(MemberWebSys.Models.MBtestContext context)
        {
            _context = context;
        }

        public IPagedList<TOfferingBatches> TOfferingBatches { get; set; }
        public string batchNo { get; set; }
        public string Posted { get; set; }
        public DateTime frmDate { get; set; }
        public DateTime toDate { get; set; }
        public int pNo { get; set; }

        public async Task<IActionResult> OnGetAsync(String batchNo, DateTime frmDate, DateTime toDate, int pageNo = 1)
        {
            this.frmDate = frmDate;
            this.toDate = toDate;
            this.batchNo = batchNo;
            int pageSize = 20;

            var items = _context.TOfferingBatches.AsQueryable();

            //Data
            if (this.frmDate != default(DateTime))
            {
                items = items.Where(o => o.Date >= this.frmDate);
            }
            if (this.toDate != default(DateTime))
            {
                items = items.Where(o => o.Date <= this.toDate);
            }
            if (this.batchNo != null)
            {
                items = items.Where(o => EF.Functions.Like(o.Time, this.batchNo + '%'));
            }

            items = items.OrderBy(o => o.Time);

            TOfferingBatches = await items.ToPagedListAsync(pageSize, pageNo);

            pNo = pageNo;

            return Page();

        }
    }
}