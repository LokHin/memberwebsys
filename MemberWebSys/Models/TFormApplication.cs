﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TFormApplication
    {
        public int Id { get; set; }
        public string PersonNo { get; set; }
        public int FormId { get; set; }
        public string Status { get; set; }
        public DateTimeOffset SubmitDate { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TForm Form { get; set; }
        public TPersonalInformation PersonNoNavigation { get; set; }
    }
}
