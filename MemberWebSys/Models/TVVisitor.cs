﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVVisitor
    {
        public TVVisitor()
        {
            TVResponse = new HashSet<TVResponse>();
            TVVisitTeam = new HashSet<TVVisitTeam>();
            TVVisitorLang = new HashSet<TVVisitorLang>();
            TVVisitorTime = new HashSet<TVVisitorTime>();
        }

        public int VisitorId { get; set; }
        public string PersonNo { get; set; }
        public string Status { get; set; }
        public string ChineseName { get; set; }
        public string EnglishName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool? Whatsapp { get; set; }
        public bool? Kakaotalk { get; set; }
        public string Occupation { get; set; }
        public string OfficeLocation { get; set; }
        public string Ministries { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string Village { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
        public bool? CellMember { get; set; }
        public string CellLeaderName { get; set; }
        public bool? OtherChurch { get; set; }
        public bool? Baptized { get; set; }
        public bool? Mgleader { get; set; }
        public string Mgcode { get; set; }
        public int? ConfessionYear { get; set; }
        public bool? NotInCell { get; set; }
        public string OtherLanguage { get; set; }
        public bool? Apostle { get; set; }
        public bool? Prophet { get; set; }
        public bool? Evangelist { get; set; }
        public bool? Pastor { get; set; }
        public bool? Teacher { get; set; }
        public bool? PropheticVisiting { get; set; }
        public bool? Visited { get; set; }
        public bool? VisitedSick { get; set; }
        public int? ConfessedClient { get; set; }
        public bool? PrayerMeeting { get; set; }
        public bool? VisitTips { get; set; }
        public bool? EvangelismTraining { get; set; }
        public int? ProphetYear { get; set; }
        public bool? ProphetI { get; set; }
        public bool? ProphetIi { get; set; }
        public int? ProphetIiscore { get; set; }
        public bool? ProphetIii { get; set; }
        public int? HealedClient { get; set; }
        public int? Frequency { get; set; }
        public string OtherFrequency { get; set; }
        public int? PositionId { get; set; }
        public string HongKongIsland { get; set; }
        public string Kowloon { get; set; }
        public string NewTerritories { get; set; }
        public string Remark { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public string Updatedby { get; set; }
        public DateTime Updatedon { get; set; }

        public ICollection<TVResponse> TVResponse { get; set; }
        public ICollection<TVVisitTeam> TVVisitTeam { get; set; }
        public ICollection<TVVisitorLang> TVVisitorLang { get; set; }
        public ICollection<TVVisitorTime> TVVisitorTime { get; set; }
    }
}
