﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TFamily
    {
        public int FamilyId { get; set; }
        public string HusbandId { get; set; }
        public string WifeId { get; set; }

        public TPersonalInformation Wife { get; set; }
    }
}
