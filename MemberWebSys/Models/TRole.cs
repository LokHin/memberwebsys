﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TRole
    {
        public TRole()
        {
            InverseParentRole = new HashSet<TRole>();
            TPermission = new HashSet<TPermission>();
            TWebUserRole = new HashSet<TWebUserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentRoleId { get; set; }
        public int ApplicationId { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TApplication Application { get; set; }
        public TRole ParentRole { get; set; }
        public ICollection<TRole> InverseParentRole { get; set; }
        public ICollection<TPermission> TPermission { get; set; }
        public ICollection<TWebUserRole> TWebUserRole { get; set; }
    }
}
