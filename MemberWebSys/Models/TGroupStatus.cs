﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroupStatus
    {
        public TGroupStatus()
        {
            TGroup = new HashSet<TGroup>();
        }

        public int Id { get; set; }
        public string NodeType { get; set; }
        public string Status { get; set; }

        public ICollection<TGroup> TGroup { get; set; }
    }
}
