﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TEventSchema
    {
        public TEventSchema()
        {
            TEvent = new HashSet<TEvent>();
            TEventSchemaRelationshipEvent = new HashSet<TEventSchemaRelationship>();
            TEventSchemaRelationshipTargetEvent = new HashSet<TEventSchemaRelationship>();
            TEventSchemaTaxinomy = new HashSet<TEventSchemaTaxinomy>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string Description { get; set; }

        public ICollection<TEvent> TEvent { get; set; }
        public ICollection<TEventSchemaRelationship> TEventSchemaRelationshipEvent { get; set; }
        public ICollection<TEventSchemaRelationship> TEventSchemaRelationshipTargetEvent { get; set; }
        public ICollection<TEventSchemaTaxinomy> TEventSchemaTaxinomy { get; set; }
    }
}
