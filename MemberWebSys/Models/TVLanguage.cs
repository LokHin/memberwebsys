﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVLanguage
    {
        public TVLanguage()
        {
            TVVisit = new HashSet<TVVisit>();
            TVVisitorLang = new HashSet<TVVisitorLang>();
        }

        public int LanguageId { get; set; }
        public string Language { get; set; }

        public ICollection<TVVisit> TVVisit { get; set; }
        public ICollection<TVVisitorLang> TVVisitorLang { get; set; }
    }
}
