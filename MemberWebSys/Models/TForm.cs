﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TForm
    {
        public TForm()
        {
            TEvent = new HashSet<TEvent>();
            TFormApplication = new HashSet<TFormApplication>();
        }

        public int Id { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public int? Quota { get; set; }
        public string Status { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public ICollection<TEvent> TEvent { get; set; }
        public ICollection<TFormApplication> TFormApplication { get; set; }
    }
}
