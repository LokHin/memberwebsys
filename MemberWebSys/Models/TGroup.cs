﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroup
    {
        public TGroup()
        {
            InverseParentGroupNoNavigation = new HashSet<TGroup>();
            TGroupMemberList = new HashSet<TGroupMemberList>();
            TMeeting = new HashSet<TMeeting>();
        }

        public int GroupNo { get; set; }
        public int? ParentGroupNo { get; set; }
        public string PersonNo { get; set; }
        public string Code { get; set; }
        public string NodeType { get; set; }
        public int? Level { get; set; }
        public string GroupName { get; set; }
        public int? GroupTypeNo { get; set; }
        public int? GroupCategoryId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string OldTime { get; set; }
        public bool? Attendance { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string HostName { get; set; }
        public string MeetingDate { get; set; }
        public string Comments { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int? StatusId { get; set; }

        public TGroupCategory GroupCategory { get; set; }
        public TGroupType GroupTypeNoNavigation { get; set; }
        public TGroup ParentGroupNoNavigation { get; set; }
        public TPersonalInformation PersonNoNavigation { get; set; }
        public TGroupStatus Status { get; set; }
        public ICollection<TGroup> InverseParentGroupNoNavigation { get; set; }
        public ICollection<TGroupMemberList> TGroupMemberList { get; set; }
        public ICollection<TMeeting> TMeeting { get; set; }
    }
}
