﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TEventVenue
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public int Id { get; set; }
    }
}
