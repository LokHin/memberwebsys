﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TTask
    {
        public TTask()
        {
            InverseParentTask = new HashSet<TTask>();
            TPermission = new HashSet<TPermission>();
        }

        public int Id { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public bool? DefaultAllow { get; set; }
        public int? ParentTaskId { get; set; }
        public int ApplicationId { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TApplication Application { get; set; }
        public TTask ParentTask { get; set; }
        public ICollection<TTask> InverseParentTask { get; set; }
        public ICollection<TPermission> TPermission { get; set; }
    }
}
