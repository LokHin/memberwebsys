﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVVisitorLang
    {
        public int VisitorId { get; set; }
        public int LanguageId { get; set; }

        public TVLanguage Language { get; set; }
        public TVVisitor Visitor { get; set; }
    }
}
