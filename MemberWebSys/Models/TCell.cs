﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TCell
    {
        public TCell()
        {
            TCellMember = new HashSet<TCellMember>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public int? GroupTypeNo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string OldTime { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string HostName { get; set; }
        public string MeetingDate { get; set; }
        public string Comments { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int? OldId { get; set; }

        public ICollection<TCellMember> TCellMember { get; set; }
    }
}
