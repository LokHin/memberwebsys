﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TAudit
    {
        public int AuditId { get; set; }
        public string FormName { get; set; }
        public string RecordId { get; set; }
        public string FieldAltered { get; set; }
        public DateTime? DateTimeAltered { get; set; }
        public string AlteredBy { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
