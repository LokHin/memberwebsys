﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TUserLog
    {
        public long Id { get; set; }
        public DateTime? Time { get; set; }
        public string UserId { get; set; }
        public string Page { get; set; }
        public string Action { get; set; }
        public string Cell { get; set; }
        public string Input { get; set; }
        public string Ip { get; set; }

        public TUser User { get; set; }
    }
}
