﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TPermission
    {
        public int RoleId { get; set; }
        public int TaskId { get; set; }
        public string Scope { get; set; }
        public bool? Permitted { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TRole Role { get; set; }
        public TTask Task { get; set; }
    }
}
