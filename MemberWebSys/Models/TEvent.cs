﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TEvent
    {
        public int Id { get; set; }
        public int? FormId { get; set; }
        public int EventSchemaId { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset? EndTime { get; set; }
        public string AllDates { get; set; }
        public string Status { get; set; }
        public string Speaker { get; set; }
        public string Venue { get; set; }
        public string TargetGroup { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TEventSchema EventSchema { get; set; }
        public TForm Form { get; set; }
    }
}
