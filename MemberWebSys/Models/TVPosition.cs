﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVPosition
    {
        public TVPosition()
        {
            TVVisitTeam = new HashSet<TVVisitTeam>();
        }

        public int PositionId { get; set; }
        public string Position { get; set; }

        public ICollection<TVVisitTeam> TVVisitTeam { get; set; }
    }
}
