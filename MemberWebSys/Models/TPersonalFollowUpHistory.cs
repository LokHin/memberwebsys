﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TPersonalFollowUpHistory
    {
        public int Id { get; set; }
        public string PersonNo { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Time { get; set; }
        public float? Duration { get; set; }
        public string Participants { get; set; }
        public string Reason { get; set; }
        public string Response { get; set; }
        public string Remarks { get; set; }

        public TPersonalInformation PersonNoNavigation { get; set; }
    }
}
