﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVResponse
    {
        public int ResponseId { get; set; }
        public int? VisitorId { get; set; }
        public int VisitId { get; set; }
        public DateTime? Date { get; set; }
        public int? Duration { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int? Love { get; set; }
        public int? Listening { get; set; }
        public int? Pray { get; set; }
        public int? Spiritual { get; set; }
        public int? Unity { get; set; }
        public int? Faith { get; set; }
        public int? SelfSatisfaction { get; set; }
        public bool? Pleased { get; set; }
        public bool? Food { get; set; }
        public bool? Cleaning { get; set; }
        public bool? Tutorial { get; set; }
        public bool? Church { get; set; }
        public string Donation { get; set; }
        public string Need { get; set; }
        public string Thanksgiving { get; set; }
        public string Difficulty { get; set; }
        public string Witness { get; set; }
        public string Question { get; set; }
        public bool? KeepContact { get; set; }
        public bool? Participant { get; set; }
        public string FollowedBy { get; set; }
        public bool? Confessed { get; set; }
        public bool? ReVisit { get; set; }
        public string Other { get; set; }
        public bool? Bappointment { get; set; }
        public DateTime? Appointment { get; set; }
        public int? AppointmentStartTime { get; set; }
        public int? AppointmentEndTime { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public string Updatedby { get; set; }
        public DateTime Updatedon { get; set; }

        public TVVisit Visit { get; set; }
        public TVVisitor Visitor { get; set; }
    }
}
