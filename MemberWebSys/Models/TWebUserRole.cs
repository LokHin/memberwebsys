﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TWebUserRole
    {
        public int RoleId { get; set; }
        public int UserId { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TRole Role { get; set; }
        public TWebUser User { get; set; }
    }
}
