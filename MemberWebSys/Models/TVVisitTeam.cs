﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVVisitTeam
    {
        public int VisitId { get; set; }
        public int VisitorId { get; set; }
        public int PositionId { get; set; }

        public TVPosition Position { get; set; }
        public TVVisit Visit { get; set; }
        public TVVisitor Visitor { get; set; }
    }
}
