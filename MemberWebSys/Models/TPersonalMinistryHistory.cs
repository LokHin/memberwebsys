﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TPersonalMinistryHistory
    {
        public int Id { get; set; }
        public string PersonNo { get; set; }
        public string MinistryArea { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Position { get; set; }
        public string ChurchName { get; set; }
        public string Remarks { get; set; }

        public TPersonalInformation PersonNoNavigation { get; set; }
    }
}
