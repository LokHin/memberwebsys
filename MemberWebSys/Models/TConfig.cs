﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TConfig
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Name { get; set; }
        public string Contents { get; set; }
    }
}
