﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TPersonalGiftingAndSkills
    {
        public int Id { get; set; }
        public string PersonNo { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }

        public TPersonalInformation PersonNoNavigation { get; set; }
    }
}
