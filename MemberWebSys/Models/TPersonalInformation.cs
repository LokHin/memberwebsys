﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MemberWebSys.Models
{
    public partial class TPersonalInformation
    {
        public TPersonalInformation()
        {
            TFormApplication = new HashSet<TFormApplication>();
            TGroup = new HashSet<TGroup>();
            TGroupMemberList = new HashSet<TGroupMemberList>();
            TMeetingAttendance = new HashSet<TMeetingAttendance>();
            TOfferEnvelope = new HashSet<TOfferEnvelope>();
            TOfferingBatch = new HashSet<TOfferingBatch>();
            TPersonalFollowUpHistory = new HashSet<TPersonalFollowUpHistory>();
            TPersonalGiftingAndSkills = new HashSet<TPersonalGiftingAndSkills>();
            TPersonalMinistryHistory = new HashSet<TPersonalMinistryHistory>();
            TPersonalTrainingHistory = new HashSet<TPersonalTrainingHistory>();
        }

        [Required]
        [Display(Name = "611編號")]
        public string PersonNo { get; set; }
        [Required]
        [Display(Name = "類別")]
        public int MemberCategory { get; set; }
        [Display(Name = "舊有編號")]
        public string OldMemberId { get; set; }
        [Display(Name = "中文姓名")]
        public string ChineseName { get; set; }
        [Display(Name = "英文姓氏")]
        public string LastName { get; set; }
        [Display(Name = "英文名字")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "顯示名稱")]
        public string DisplayName { get; set; }
        [Display(Name = "稱謂")]
        public string Salutation { get; set; }
        public bool? EnglishAddress { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        [Display(Name = "住宅地址")]
        public string Address { get; set; }
        public bool? SameAddress { get; set; }
        public string MailRegion { get; set; }
        public string MailDistrict { get; set; }
        [Display(Name = "郵寄地址")]
        public string MailAddress { get; set; }
        [Display(Name = "住宅電話")]
        public string HomePhone { get; set; }
        [Display(Name = "公司電話")]
        public string WorkPhone { get; set; }
        [Display(Name = "手提電話")]
        public string MobilePhone { get; set; }
        [Display(Name = "住宅傳真")]
        public string HomeFax { get; set; }
        [Display(Name = "公司傳真")]
        public string WorkFax { get; set; }
        [Display(Name = "電郵地址")]
        public string Email { get; set; }
        [Display(Name = "性別")]
        [RegularExpression(@"M|F|null")]
        public string Gender { get; set; }
        [Display(Name = "出生日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Dob { get; set; }
        [Display(Name = "出生地點")]
        public string PlaceOfBirth { get; set; }
        [Display(Name = "婚姻狀況")]
        public string MaritalStatus { get; set; }
        [Display(Name = "結婚日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? MarriageDate { get; set; }
        public int? ParentId { get; set; }
        [Display(Name = "去世日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DeathDate { get; set; }
        public string Disability { get; set; }
        [Display(Name = "常用語言")]
        public string MotherTongue { get; set; }
        [Display(Name = "其他語言")]
        public string Dialect { get; set; }
        [Display(Name = "職業類別")]
        public string OccupationType { get; set; }
        [Display(Name = "職業")]
        public string Occupation { get; set; }
        [Display(Name = "個人月入")]
        public string Salary { get; set; }
        [Display(Name = "公司/學校")]
        public string CompanyName { get; set; }
        [Display(Name = "藉貫")]
        public string Origin { get; set; }
        [Display(Name = "國藉")]
        public string Nationality { get; set; }
        [Display(Name = "證件號碼")]
        public string IdNo { get; set; }
        public string Immigration { get; set; }
        [Display(Name = "教育程度")]
        public string Education { get; set; }
        [Display(Name = "決志日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ConvictionDate { get; set; }
        [Display(Name = "參加本教會日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? AttendDate { get; set; }
        [Display(Name = "受浸日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? WaterBaptismDate { get; set; }
        [Display(Name = "受浸教會")]
        public string WaterBaptismChurch { get; set; }
        [Display(Name = "入會日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? JoinDate { get; set; }
        [Display(Name = "離開日期")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? LeaveDate { get; set; }
        [Display(Name = "原屬教會")]
        public string PreviousChurchName { get; set; }
        public string EmergencyContact { get; set; }
        public string EmergencyPhone { get; set; }
        public string EmergencyRelation { get; set; }
        [Display(Name = "備註")]
        public string Comment { get; set; }
        [Display(Name = "聯名奉獻")]
        public string CombinedOffering { get; set; }
        [Display(Name = "奉獻編號")]
        public string EnvelopeNo { get; set; }
        [Display(Name = "奉獻姓名")]
        public string OfferingName { get; set; }
        [Display(Name = "收據姓名")]
        public string ReceiptName { get; set; }
        public DateTime? LastUpdate { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? InputDate { get; set; }
        public string CustomText1 { get; set; }
        public string CustomText2 { get; set; }
        public string CustomText3 { get; set; }
        public string CustomText4 { get; set; }
        public string CustomText5 { get; set; }
        public DateTime? CustomDate1 { get; set; }
        public DateTime? CustomDate2 { get; set; }
        public DateTime? CustomDate3 { get; set; }
        public DateTime? CustomDate4 { get; set; }
        public DateTime? CustomDate5 { get; set; }
        public int? CustomNumber1 { get; set; }
        public int? CustomNumber2 { get; set; }
        public int? CustomNumber3 { get; set; }
        public int? CustomNumber4 { get; set; }
        public int? CustomNumber5 { get; set; }
        public string PpsNo { get; set; }
        public byte[] Photo { get; set; }
        public int? ProfileId { get; set; }
        [Display(Name = "施浸牧師")]
        public string WaterBaptismPastor { get; set; }
        public int? BaptismYear { get; set; }
        public int? BaptismMonth { get; set; }
        public int? BaptismDay { get; set; }
        public int? ConvictionYear { get; set; }
        public int? ConvictionMonth { get; set; }
        public int? ConvictionDay { get; set; }
        public int? BirthYear { get; set; }
        public int? BirthMonth { get; set; }
        public int? BirthDay { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public DateTime? LoginLast { get; set; }
        public int? LoginCount { get; set; }
        [Required]
        public bool? AllowLogin { get; set; }
        public int? UserId { get; set; }
        public string CellLeaderId { get; set; }
        public bool? HasCell { get; set; }
        [Display(Name = "名冊編號")]
        public int? MemberId { get; set; }
        [Display(Name = "同工")]
        public bool IsStaff { get; set; }
        [Display(Name = "類別")]
        public TMemberCategory MemberCategoryNavigation { get; set; }
        public TWebUser User { get; set; }
        public TFamily TFamily { get; set; }
        public ICollection<TFormApplication> TFormApplication { get; set; }
        public ICollection<TGroup> TGroup { get; set; }
        public ICollection<TGroupMemberList> TGroupMemberList { get; set; }
        public ICollection<TMeetingAttendance> TMeetingAttendance { get; set; }
        public ICollection<TOfferEnvelope> TOfferEnvelope { get; set; }
        public ICollection<TOfferingBatch> TOfferingBatch { get; set; }
        public ICollection<TPersonalFollowUpHistory> TPersonalFollowUpHistory { get; set; }
        public ICollection<TPersonalGiftingAndSkills> TPersonalGiftingAndSkills { get; set; }
        public ICollection<TPersonalMinistryHistory> TPersonalMinistryHistory { get; set; }
        public ICollection<TPersonalTrainingHistory> TPersonalTrainingHistory { get; set; }
    }
}
