﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TOfferingItems
    {
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public int InputSeq { get; set; }
        public short OfferingTypeId { get; set; }
        public decimal? Amount { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string EnvelopeId { get; set; }

        public TOfferingType OfferingType { get; set; }
        public TOfferingBatch TOfferingBatch { get; set; }
    }
}
