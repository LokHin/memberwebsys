﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TMemberCategory
    {
        public TMemberCategory()
        {
            TPersonalInformation = new HashSet<TPersonalInformation>();
        }

        public int MemberCategoryId { get; set; }
        public string MemberCategory { get; set; }

        public ICollection<TPersonalInformation> TPersonalInformation { get; set; }
    }
}
