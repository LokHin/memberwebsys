﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TPersonalTrainingHistory
    {
        public int Id { get; set; }
        public string PersonNo { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Category { get; set; }
        public string Institute { get; set; }
        public string Certificate { get; set; }
        public string Remarks { get; set; }

        public TPersonalInformation PersonNoNavigation { get; set; }
    }
}
