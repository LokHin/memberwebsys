﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroupMemberList
    {
        public int GroupNo { get; set; }
        public string PersonNo { get; set; }
        public int? Type { get; set; }
        public int? Position { get; set; }
        public DateTime? JoinDate { get; set; }
        public DateTime? LeaveDate { get; set; }
        public string Remarks { get; set; }
        public string AllowedEventCodes { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public TGroup GroupNoNavigation { get; set; }
        public TPersonalInformation PersonNoNavigation { get; set; }
        public TGroupMemberPosition PositionNavigation { get; set; }
    }
}
