﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TMgMember
    {
        public TMgMember()
        {
            TCellMember = new HashSet<TCellMember>();
        }

        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string PersonNo { get; set; }
        public string Code { get; set; }
        public int? Level { get; set; }
        public string Remark { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public int? OldId { get; set; }
        public string OldGroupName { get; set; }

        public ICollection<TCellMember> TCellMember { get; set; }
    }
}
