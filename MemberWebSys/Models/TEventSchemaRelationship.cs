﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TEventSchemaRelationship
    {
        public int EventId { get; set; }
        public int TargetEventId { get; set; }
        public string Type { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TEventSchema Event { get; set; }
        public TEventSchema TargetEvent { get; set; }
    }
}
