﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TApplication
    {
        public TApplication()
        {
            TRole = new HashSet<TRole>();
            TTask = new HashSet<TTask>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public ICollection<TRole> TRole { get; set; }
        public ICollection<TTask> TTask { get; set; }
    }
}
