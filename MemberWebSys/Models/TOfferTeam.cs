﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TOfferTeam
    {
        public TOfferTeam()
        {
            TOfferEnvelope = new HashSet<TOfferEnvelope>();
        }

        public string TeamId { get; set; }
        public int LastNum { get; set; }

        public ICollection<TOfferEnvelope> TOfferEnvelope { get; set; }
    }
}
