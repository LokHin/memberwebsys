﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MemberWebSys.Models
{
    public class JsonPersonInfo
    {
        
        [Display(Name = "611編號")]
        public string PersonNo { get; set; }
        [Display(Name = "類別")]
        public int MemberCategory { get; set; }
        [Display(Name = "中文姓名")]
        public string ChineseName { get; set; }
        [Display(Name = "英文姓氏")]
        public string LastName { get; set; }
        [Display(Name = "英文名字")]
        public string OfferingName { get; set; }
        [Display(Name = "名字")]
        public string FirstName { get; set; }
        [Display(Name = "顯示名稱")]
        public string DisplayName { get; set; }
        [Display(Name = "稱謂")]
        public string Salutation { get; set; }
        [Display(Name = "住宅電話")]
        public string HomePhone { get; set; }
        [Display(Name = "公司電話")]
        public string WorkPhone { get; set; }
        [Display(Name = "手提電話")]
        public string MobilePhone { get; set; }
    }
}
