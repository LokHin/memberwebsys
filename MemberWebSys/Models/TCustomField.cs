﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TCustomField
    {
        public string FieldName { get; set; }
        public string FieldHeader { get; set; }
    }
}
