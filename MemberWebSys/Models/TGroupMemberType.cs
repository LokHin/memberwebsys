﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroupMemberType
    {
        public int GroupMemberTypeId { get; set; }
        public string GroupMemberType { get; set; }
    }
}
