﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVRegistrant
    {
        public TVRegistrant()
        {
            TVVisit = new HashSet<TVVisit>();
        }

        public int RegistrantId { get; set; }
        public string PersonNo { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string CellLeaderName { get; set; }
        public bool? NotInCell { get; set; }
        public string OtherChurch { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public string Updatedby { get; set; }
        public DateTime Updatedon { get; set; }

        public ICollection<TVVisit> TVVisit { get; set; }
    }
}
