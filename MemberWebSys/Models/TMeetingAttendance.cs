﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TMeetingAttendance
    {
        public int MeetingId { get; set; }
        public string PersonNo { get; set; }
        public bool? Attended { get; set; }
        public string Reason { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public TMeeting Meeting { get; set; }
        public TPersonalInformation PersonNoNavigation { get; set; }
    }
}
