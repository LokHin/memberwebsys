﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MemberWebSys.Models
{
    public partial class TOfferingBatches : BaseEntity
    {
        public TOfferingBatches()
        {
            TOfferEnvelope = new HashSet<TOfferEnvelope>();
            TOfferingBatch = new HashSet<TOfferingBatch>();
        }
        [Display(Name = "日期")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Display(Name = "批次")]
        public string Time { get; set; }
        [Display(Name = "名稱")]
        public string BatchName { get; set; }
        public string Posted { get; set; }
        [NotMapped]
        [Display(Name ="已核對")]
        public bool PostedBool { get { return this.Posted == "Y"; } set { this.Posted = (value ? "Y" : "N"); } }
        

        public ICollection<TOfferEnvelope> TOfferEnvelope { get; set; }
        public ICollection<TOfferingBatch> TOfferingBatch { get; set; }
    }
}
