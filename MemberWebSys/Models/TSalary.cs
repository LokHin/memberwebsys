﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TSalary
    {
        public int SalaryId { get; set; }
        public string SalaryRange { get; set; }
    }
}
