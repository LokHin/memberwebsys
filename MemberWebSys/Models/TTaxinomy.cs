﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TTaxinomy
    {
        public TTaxinomy()
        {
            InverseParentNavigation = new HashSet<TTaxinomy>();
            TEventSchemaTaxinomy = new HashSet<TEventSchemaTaxinomy>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public int? Parent { get; set; }

        public TTaxinomy ParentNavigation { get; set; }
        public ICollection<TTaxinomy> InverseParentNavigation { get; set; }
        public ICollection<TEventSchemaTaxinomy> TEventSchemaTaxinomy { get; set; }
    }
}
