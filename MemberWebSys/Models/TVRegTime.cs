﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVRegTime
    {
        public int VisitId { get; set; }
        public int Date { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }

        public TVVisit Visit { get; set; }
    }
}
