﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MemberWebSys.Models
{
    public class BaseEntity
    {
        [Display(Name = "最後修改")]
        public string LastUpdateBy { get; set; }
        [Display(Name = "最後修改時間")]
        public DateTime? LastUpdateTime { get; set; }
        [Display(Name = "建立者")]
        public string CreatedBy { get; set; }
        [Display(Name = "建立時間")]
        public DateTime? CreatedTime { get; set; }
    }
}
