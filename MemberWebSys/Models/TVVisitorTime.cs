﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVVisitorTime
    {
        public int VisitorId { get; set; }
        public int Date { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }

        public TVVisitor Visitor { get; set; }
    }
}
