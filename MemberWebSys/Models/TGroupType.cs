﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroupType
    {
        public TGroupType()
        {
            TGroup = new HashSet<TGroup>();
        }

        public int GroupTypeNo { get; set; }
        public string GroupType { get; set; }

        public ICollection<TGroup> TGroup { get; set; }
    }
}
