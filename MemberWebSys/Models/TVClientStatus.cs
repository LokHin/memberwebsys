﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVClientStatus
    {
        public int VisitId { get; set; }
        public int ClientId { get; set; }
        public int? Age { get; set; }
        public string Relationship { get; set; }
        public string Status { get; set; }
        public string Illness { get; set; }
        public bool? Bvisited { get; set; }
        public DateTime? Visited { get; set; }
        public string Conviction { get; set; }
        public bool? Sprinkling { get; set; }
        public string Remarks { get; set; }

        public TVClient Client { get; set; }
        public TVVisit Visit { get; set; }
    }
}
