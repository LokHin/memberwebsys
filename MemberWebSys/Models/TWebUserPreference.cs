﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TWebUserPreference
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TWebUser User { get; set; }
    }
}
