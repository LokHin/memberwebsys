﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MemberWebSys.Models
{
    public partial class MBtestContext : DbContext
    {
        public virtual DbSet<TApplication> TApplication { get; set; }
        public virtual DbSet<TAudit> TAudit { get; set; }
        public virtual DbSet<TCell> TCell { get; set; }
        public virtual DbSet<TCellMember> TCellMember { get; set; }
        public virtual DbSet<TConfig> TConfig { get; set; }
        public virtual DbSet<TCustomField> TCustomField { get; set; }
        public virtual DbSet<TEvent> TEvent { get; set; }
        public virtual DbSet<TEventSchema> TEventSchema { get; set; }
        public virtual DbSet<TEventSchemaRelationship> TEventSchemaRelationship { get; set; }
        public virtual DbSet<TEventSchemaTaxinomy> TEventSchemaTaxinomy { get; set; }
        public virtual DbSet<TEventVenue> TEventVenue { get; set; }
        public virtual DbSet<TFamily> TFamily { get; set; }
        public virtual DbSet<TForm> TForm { get; set; }
        public virtual DbSet<TFormApplication> TFormApplication { get; set; }
        public virtual DbSet<TGroup> TGroup { get; set; }
        public virtual DbSet<TGroupCategory> TGroupCategory { get; set; }
        public virtual DbSet<TGroupMemberList> TGroupMemberList { get; set; }
        public virtual DbSet<TGroupMemberPosition> TGroupMemberPosition { get; set; }
        public virtual DbSet<TGroupMemberType> TGroupMemberType { get; set; }
        public virtual DbSet<TGroupStatus> TGroupStatus { get; set; }
        public virtual DbSet<TGroupType> TGroupType { get; set; }
        public virtual DbSet<TMeeting> TMeeting { get; set; }
        public virtual DbSet<TMeetingAttendance> TMeetingAttendance { get; set; }
        public virtual DbSet<TMemberCategory> TMemberCategory { get; set; }
        public virtual DbSet<TMgMember> TMgMember { get; set; }
        public virtual DbSet<TOccupationType> TOccupationType { get; set; }
        public virtual DbSet<TOfferEnvelope> TOfferEnvelope { get; set; }
        public virtual DbSet<TOfferEnvelopeItems> TOfferEnvelopeItems { get; set; }
        public virtual DbSet<TOfferingBatch> TOfferingBatch { get; set; }
        public virtual DbSet<TOfferingBatches> TOfferingBatches { get; set; }
        public virtual DbSet<TOfferingItems> TOfferingItems { get; set; }
        public virtual DbSet<TOfferingType> TOfferingType { get; set; }
        public virtual DbSet<TOfferTeam> TOfferTeam { get; set; }
        public virtual DbSet<TPermission> TPermission { get; set; }
        public virtual DbSet<TPersonalFollowUpHistory> TPersonalFollowUpHistory { get; set; }
        public virtual DbSet<TPersonalGiftingAndSkills> TPersonalGiftingAndSkills { get; set; }
        public virtual DbSet<TPersonalInformation> TPersonalInformation { get; set; }
        public virtual DbSet<TPersonalMinistryHistory> TPersonalMinistryHistory { get; set; }
        public virtual DbSet<TPersonalTrainingHistory> TPersonalTrainingHistory { get; set; }
        public virtual DbSet<TRole> TRole { get; set; }
        public virtual DbSet<TSalary> TSalary { get; set; }
        public virtual DbSet<TTask> TTask { get; set; }
        public virtual DbSet<TTaxinomy> TTaxinomy { get; set; }
        public virtual DbSet<TUser> TUser { get; set; }
        public virtual DbSet<TUserLog> TUserLog { get; set; }
        public virtual DbSet<TVClient> TVClient { get; set; }
        public virtual DbSet<TVClientStatus> TVClientStatus { get; set; }
        public virtual DbSet<TVLanguage> TVLanguage { get; set; }
        public virtual DbSet<TVPosition> TVPosition { get; set; }
        public virtual DbSet<TVRegistrant> TVRegistrant { get; set; }
        public virtual DbSet<TVRegTime> TVRegTime { get; set; }
        public virtual DbSet<TVResponse> TVResponse { get; set; }
        public virtual DbSet<TVVisit> TVVisit { get; set; }
        public virtual DbSet<TVVisitor> TVVisitor { get; set; }
        public virtual DbSet<TVVisitorLang> TVVisitorLang { get; set; }
        public virtual DbSet<TVVisitorTime> TVVisitorTime { get; set; }
        public virtual DbSet<TVVisitTeam> TVVisitTeam { get; set; }
        public virtual DbSet<TWebUser> TWebUser { get; set; }
        public virtual DbSet<TWebUserPreference> TWebUserPreference { get; set; }
        public virtual DbSet<TWebUserRole> TWebUserRole { get; set; }

        // Unable to generate entity type for table 'dbo.TEMP'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tChurch'. Please see the warning messages.

        public MBtestContext(DbContextOptions<MBtestContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TApplication>(entity =>
            {
                entity.ToTable("tApplication", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ClientId)
                    .HasColumnName("client_id")
                    .HasMaxLength(255);

                entity.Property(e => e.ClientSecret)
                    .HasColumnName("client_secret")
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TAudit>(entity =>
            {
                entity.HasKey(e => e.AuditId);

                entity.ToTable("tAudit");

                entity.HasIndex(e => e.AuditId)
                    .HasName("tAudit$AuditID");

                entity.HasIndex(e => e.RecordId)
                    .HasName("tAudit$Record_ID");

                entity.Property(e => e.AuditId).HasColumnName("AuditID");

                entity.Property(e => e.AlteredBy).HasMaxLength(255);

                entity.Property(e => e.DateTimeAltered).HasColumnType("datetime2(0)");

                entity.Property(e => e.FieldAltered).HasMaxLength(255);

                entity.Property(e => e.FormName).HasMaxLength(255);

                entity.Property(e => e.From).HasMaxLength(255);

                entity.Property(e => e.RecordId)
                    .HasColumnName("RecordID")
                    .HasMaxLength(50);

                entity.Property(e => e.To).HasMaxLength(255);
            });

            modelBuilder.Entity<TCell>(entity =>
            {
                entity.ToTable("tCell");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Comments).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("End Date")
                    .HasColumnType("date");

                entity.Property(e => e.EndTime).HasColumnName("End Time");

                entity.Property(e => e.GroupName)
                    .HasColumnName("Group Name")
                    .HasMaxLength(50);

                entity.Property(e => e.GroupTypeNo).HasColumnName("Group Type No");

                entity.Property(e => e.HostName)
                    .HasColumnName("Host Name")
                    .HasMaxLength(30);

                entity.Property(e => e.Location).HasMaxLength(50);

                entity.Property(e => e.MeetingDate)
                    .HasColumnName("Meeting Date")
                    .HasMaxLength(10);

                entity.Property(e => e.OldId).HasColumnName("OldID");

                entity.Property(e => e.OldTime)
                    .HasColumnName("Old_Time")
                    .HasMaxLength(255);

                entity.Property(e => e.StartDate)
                    .HasColumnName("Start Date")
                    .HasColumnType("date");

                entity.Property(e => e.StartTime).HasColumnName("Start Time");

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<TCellMember>(entity =>
            {
                entity.HasKey(e => new { e.MgMemberId, e.CellId });

                entity.ToTable("tCell_Member");

                entity.Property(e => e.MgMemberId).HasColumnName("MG Member ID");

                entity.Property(e => e.CellId).HasColumnName("CellID");

                entity.Property(e => e.JoinDate)
                    .HasColumnName("Join Date")
                    .HasColumnType("date");

                entity.Property(e => e.LeaveDate)
                    .HasColumnName("Leave Date")
                    .HasColumnType("date");

                entity.Property(e => e.Position).HasColumnName("position");

                entity.Property(e => e.Remarks).HasMaxLength(255);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Cell)
                    .WithMany(p => p.TCellMember)
                    .HasForeignKey(d => d.CellId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tCell_Member_tCell");

                entity.HasOne(d => d.MgMember)
                    .WithMany(p => p.TCellMember)
                    .HasForeignKey(d => d.MgMemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tCell_Member_tMG_Member");
            });

            modelBuilder.Entity<TConfig>(entity =>
            {
                entity.ToTable("tConfig");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.SubCategory).HasMaxLength(50);
            });

            modelBuilder.Entity<TCustomField>(entity =>
            {
                entity.HasKey(e => e.FieldName);

                entity.ToTable("tCustom_Field");

                entity.Property(e => e.FieldName)
                    .HasColumnName("Field Name")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.FieldHeader)
                    .HasColumnName("Field Header")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TEvent>(entity =>
            {
                entity.ToTable("tEvent", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AllDates)
                    .HasColumnName("all_dates")
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.EndTime).HasColumnName("end_time");

                entity.Property(e => e.EventSchemaId).HasColumnName("event_schema_id");

                entity.Property(e => e.FormId).HasColumnName("form_id");

                entity.Property(e => e.Speaker)
                    .HasColumnName("speaker")
                    .HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.StartTime).HasColumnName("start_time");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(50);

                entity.Property(e => e.TargetGroup).HasColumnName("target_group");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Venue)
                    .HasColumnName("venue")
                    .HasMaxLength(255);

                entity.HasOne(d => d.EventSchema)
                    .WithMany(p => p.TEvent)
                    .HasForeignKey(d => d.EventSchemaId)
                    .HasConstraintName("FK_tEvent_tEventSchema");

                entity.HasOne(d => d.Form)
                    .WithMany(p => p.TEvent)
                    .HasForeignKey(d => d.FormId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_tEvent_tForm");
            });

            modelBuilder.Entity<TEventSchema>(entity =>
            {
                entity.ToTable("tEventSchema", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");
            });

            modelBuilder.Entity<TEventSchemaRelationship>(entity =>
            {
                entity.HasKey(e => new { e.EventId, e.TargetEventId });

                entity.ToTable("tEventSchemaRelationship", "ecoa");

                entity.Property(e => e.EventId).HasColumnName("event_id");

                entity.Property(e => e.TargetEventId).HasColumnName("target_event_id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Event)
                    .WithMany(p => p.TEventSchemaRelationshipEvent)
                    .HasForeignKey(d => d.EventId)
                    .HasConstraintName("FK_tCourseRelationship_tCourse");

                entity.HasOne(d => d.TargetEvent)
                    .WithMany(p => p.TEventSchemaRelationshipTargetEvent)
                    .HasForeignKey(d => d.TargetEventId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tCourseRelationship_tCourse1");
            });

            modelBuilder.Entity<TEventSchemaTaxinomy>(entity =>
            {
                entity.HasKey(e => new { e.EventSchemaId, e.TaxinomyId });

                entity.ToTable("tEventSchema_Taxinomy", "ecoa");

                entity.Property(e => e.EventSchemaId).HasColumnName("event_schema_id");

                entity.Property(e => e.TaxinomyId).HasColumnName("taxinomy_id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.HasOne(d => d.EventSchema)
                    .WithMany(p => p.TEventSchemaTaxinomy)
                    .HasForeignKey(d => d.EventSchemaId)
                    .HasConstraintName("FK_tEventSchema_Taxinomy_tEventSchema");

                entity.HasOne(d => d.Taxinomy)
                    .WithMany(p => p.TEventSchemaTaxinomy)
                    .HasForeignKey(d => d.TaxinomyId)
                    .HasConstraintName("FK_tEventSchema_Taxinomy_tTaxinomy");
            });

            modelBuilder.Entity<TEventVenue>(entity =>
            {
                entity.ToTable("tEventVenue");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Capacity).HasColumnName("capacity");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<TFamily>(entity =>
            {
                entity.HasKey(e => e.FamilyId);

                entity.ToTable("tFamily");

                entity.HasIndex(e => e.FamilyId)
                    .HasName("tFamily$Family ID");

                entity.HasIndex(e => e.HusbandId)
                    .HasName("tFamily$tPersonal_InformationtFamily1")
                    .IsUnique();

                entity.HasIndex(e => e.WifeId)
                    .HasName("tFamily$tPersonal_InformationtFamily")
                    .IsUnique();

                entity.Property(e => e.FamilyId).HasColumnName("Family ID");

                entity.Property(e => e.HusbandId)
                    .HasColumnName("Husband ID")
                    .HasMaxLength(10);

                entity.Property(e => e.WifeId)
                    .HasColumnName("Wife ID")
                    .HasMaxLength(10);

                entity.HasOne(d => d.Wife)
                    .WithOne(p => p.TFamily)
                    .HasForeignKey<TFamily>(d => d.WifeId)
                    .HasConstraintName("tFamily$tPersonal_InformationtFamily");
            });

            modelBuilder.Entity<TForm>(entity =>
            {
                entity.ToTable("tForm", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.EndDate).HasColumnName("end_date");

                entity.Property(e => e.Quota).HasColumnName("quota");

                entity.Property(e => e.StartDate).HasColumnName("start_date");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("(N'draft')");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");
            });

            modelBuilder.Entity<TFormApplication>(entity =>
            {
                entity.HasKey(e => new { e.PersonNo, e.FormId });

                entity.ToTable("tForm_Application", "ecoa");

                entity.Property(e => e.PersonNo)
                    .HasColumnName("person_no")
                    .HasMaxLength(10);

                entity.Property(e => e.FormId).HasColumnName("form_id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubmitDate).HasColumnName("submit_date");

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.HasOne(d => d.Form)
                    .WithMany(p => p.TFormApplication)
                    .HasForeignKey(d => d.FormId)
                    .HasConstraintName("FK_tApplication_Form_tForm");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TFormApplication)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tApplication_Form_tPersonal_Information");
            });

            modelBuilder.Entity<TGroup>(entity =>
            {
                entity.HasKey(e => e.GroupNo);

                entity.ToTable("tGroup");

                entity.HasIndex(e => e.GroupName)
                    .HasName("tGroup$Group Name");

                entity.HasIndex(e => e.GroupTypeNo)
                    .HasName("tGroup${272EA63F-A1CB-40A1-84D0-7858B7AE6D0A}");

                entity.HasIndex(e => e.ParentGroupNo)
                    .HasName("tGroup$tGroupParent Group No");

                entity.Property(e => e.GroupNo).HasColumnName("Group No");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Code).HasMaxLength(255);

                entity.Property(e => e.Comments).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("End Date")
                    .HasColumnType("date");

                entity.Property(e => e.EndTime)
                    .HasColumnName("End Time")
                    .HasColumnType("time(0)");

                entity.Property(e => e.GroupCategoryId).HasColumnName("Group Category ID");

                entity.Property(e => e.GroupName)
                    .HasColumnName("Group Name")
                    .HasMaxLength(50);

                entity.Property(e => e.GroupTypeNo).HasColumnName("Group Type No");

                entity.Property(e => e.HostName)
                    .HasColumnName("Host Name")
                    .HasMaxLength(30);

                entity.Property(e => e.Level).HasColumnName("level");

                entity.Property(e => e.Location).HasMaxLength(50);

                entity.Property(e => e.MeetingDate)
                    .HasColumnName("Meeting Date")
                    .HasMaxLength(10);

                entity.Property(e => e.NodeType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OldTime)
                    .HasColumnName("Old_Time")
                    .HasMaxLength(255);

                entity.Property(e => e.ParentGroupNo).HasColumnName("Parent Group No");

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.StartDate)
                    .HasColumnName("Start Date")
                    .HasColumnType("date");

                entity.Property(e => e.StartTime)
                    .HasColumnName("Start Time")
                    .HasColumnType("time(0)");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.GroupCategory)
                    .WithMany(p => p.TGroup)
                    .HasForeignKey(d => d.GroupCategoryId)
                    .HasConstraintName("FK_tGroup_tGroup_Category");

                entity.HasOne(d => d.GroupTypeNoNavigation)
                    .WithMany(p => p.TGroup)
                    .HasForeignKey(d => d.GroupTypeNo)
                    .HasConstraintName("tGroup${272EA63F-A1CB-40A1-84D0-7858B7AE6D0A}");

                entity.HasOne(d => d.ParentGroupNoNavigation)
                    .WithMany(p => p.InverseParentGroupNoNavigation)
                    .HasForeignKey(d => d.ParentGroupNo)
                    .HasConstraintName("tGroup${24FB83CD-D6E0-440A-A196-1B890066DE35}");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TGroup)
                    .HasForeignKey(d => d.PersonNo)
                    .HasConstraintName("FK_tGroup_tPersonal_Information");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TGroup)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_tGroup_tGroup_Status");
            });

            modelBuilder.Entity<TGroupCategory>(entity =>
            {
                entity.ToTable("tGroup_Category");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code).HasMaxLength(10);

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<TGroupMemberList>(entity =>
            {
                entity.HasKey(e => new { e.GroupNo, e.PersonNo });

                entity.ToTable("tGroup_Member_List");

                entity.HasIndex(e => e.GroupNo)
                    .HasName("tGroup_Member_List${345D1756-B244-4460-BF29-9B5A0B62C2F9}");

                entity.HasIndex(e => e.PersonNo)
                    .HasName("tGroup_Member_List$tGroup_Member_ListPerson No");

                entity.HasIndex(e => e.Position)
                    .HasName("tGroup_Member_List${4B4E373B-677E-42D0-9724-33A0B15F386B}");

                entity.Property(e => e.GroupNo).HasColumnName("Group No");

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.JoinDate)
                    .HasColumnName("Join Date")
                    .HasColumnType("date");

                entity.Property(e => e.LeaveDate)
                    .HasColumnName("Leave Date")
                    .HasColumnType("date");

                entity.Property(e => e.Remarks).HasMaxLength(255);

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.GroupNoNavigation)
                    .WithMany(p => p.TGroupMemberList)
                    .HasForeignKey(d => d.GroupNo)
                    .HasConstraintName("tGroup_Member_List${345D1756-B244-4460-BF29-9B5A0B62C2F9}");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TGroupMemberList)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tGroup_Member_List${379AB88A-0A1A-47CB-B5CB-F62F4E78A95F}");

                entity.HasOne(d => d.PositionNavigation)
                    .WithMany(p => p.TGroupMemberList)
                    .HasForeignKey(d => d.Position)
                    .HasConstraintName("tGroup_Member_List${4B4E373B-677E-42D0-9724-33A0B15F386B}");
            });

            modelBuilder.Entity<TGroupMemberPosition>(entity =>
            {
                entity.HasKey(e => e.GroupMemberPositionId);

                entity.ToTable("tGroup_Member_Position");

                entity.HasIndex(e => e.GroupMemberPositionId)
                    .HasName("tGroup_Member_Position$ID");

                entity.Property(e => e.GroupMemberPositionId).HasColumnName("Group Member Position ID");

                entity.Property(e => e.GroupMemberPosition)
                    .HasColumnName("Group Member Position")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TGroupMemberType>(entity =>
            {
                entity.HasKey(e => e.GroupMemberTypeId);

                entity.ToTable("tGroup_Member_Type");

                entity.HasIndex(e => e.GroupMemberTypeId)
                    .HasName("tGroup_Member_Type$ID");

                entity.Property(e => e.GroupMemberTypeId).HasColumnName("Group Member Type ID");

                entity.Property(e => e.GroupMemberType)
                    .HasColumnName("Group Member Type")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TGroupStatus>(entity =>
            {
                entity.ToTable("tGroup_Status");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NodeType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TGroupType>(entity =>
            {
                entity.HasKey(e => e.GroupTypeNo);

                entity.ToTable("tGroup_Type");

                entity.Property(e => e.GroupTypeNo).HasColumnName("Group Type No");

                entity.Property(e => e.GroupType)
                    .HasColumnName("Group Type")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TMeeting>(entity =>
            {
                entity.HasKey(e => e.MeetingId);

                entity.ToTable("tMeeting");

                entity.Property(e => e.MeetingId).HasColumnName("Meeting ID");

                entity.Property(e => e.AttendTotal).HasColumnName("Attend Total");

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.GroupNo).HasColumnName("Group No");

                entity.Property(e => e.MeetingDate)
                    .HasColumnName("Meeting Date")
                    .HasColumnType("date");

                entity.Property(e => e.MeetingTime)
                    .HasColumnName("Meeting Time")
                    .HasColumnType("time(0)");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.VisitorTotal).HasColumnName("Visitor Total");

                entity.Property(e => e.Visitors).HasMaxLength(255);

                entity.HasOne(d => d.GroupNoNavigation)
                    .WithMany(p => p.TMeeting)
                    .HasForeignKey(d => d.GroupNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tMeeting_tGroup");
            });

            modelBuilder.Entity<TMeetingAttendance>(entity =>
            {
                entity.HasKey(e => new { e.MeetingId, e.PersonNo });

                entity.ToTable("tMeeting_Attendance");

                entity.Property(e => e.MeetingId).HasColumnName("Meeting ID");

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Attended).HasDefaultValueSql("((-1))");

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Reason).HasMaxLength(50);

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Meeting)
                    .WithMany(p => p.TMeetingAttendance)
                    .HasForeignKey(d => d.MeetingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tMeeting_Attendance${4B3D39FC-C574-4E47-B7CD-FE9D63A00317}");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TMeetingAttendance)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tMeeting_Attendance_tPersonal_Information");
            });

            modelBuilder.Entity<TMemberCategory>(entity =>
            {
                entity.HasKey(e => e.MemberCategoryId);

                entity.ToTable("tMember_Category");

                entity.HasIndex(e => e.MemberCategoryId)
                    .HasName("tMember_Category$Member Category ID");

                entity.Property(e => e.MemberCategoryId)
                    .HasColumnName("Member Category ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.MemberCategory)
                    .HasColumnName("Member Category")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TMgMember>(entity =>
            {
                entity.ToTable("tMG_Member");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Level).HasColumnName("level");

                entity.Property(e => e.OldGroupName).HasMaxLength(50);

                entity.Property(e => e.OldId).HasColumnName("OldID");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Remark).HasMaxLength(255);

                entity.Property(e => e.UpdatedBy).HasMaxLength(255);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<TOccupationType>(entity =>
            {
                entity.HasKey(e => e.OccTypeId);

                entity.ToTable("tOccupationType");

                entity.HasIndex(e => e.OccTypeId)
                    .HasName("tOccupationType$Salary ID");

                entity.Property(e => e.OccTypeId).HasColumnName("Occ Type  ID");

                entity.Property(e => e.OccupationType)
                    .HasColumnName("Occupation Type")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TOfferEnvelope>(entity =>
            {
                entity.HasKey(e => new { e.Date, e.Time, e.EnvelopeId });

                entity.ToTable("tOffer_Envelope");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Time).HasMaxLength(8);

                entity.Property(e => e.EnvelopeId)
                    .HasColumnName("EnvelopeID")
                    .HasMaxLength(10);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.EnvelopeNo)
                    .HasColumnName("Envelope No")
                    .HasMaxLength(10);

                entity.Property(e => e.LastUpdateBy).HasMaxLength(50);

                entity.Property(e => e.LastUpdateTime).HasColumnType("datetime");

                entity.Property(e => e.OfferingName)
                    .HasColumnName("Offering Name")
                    .HasMaxLength(50);

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Posted).HasColumnType("char(1)");

                entity.Property(e => e.Remarks).HasMaxLength(50);

                entity.Property(e => e.TeamId)
                    .IsRequired()
                    .HasColumnName("TeamID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmt).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.TransactionNo).HasMaxLength(50);

                entity.Property(e => e.TransactionType)
                    .HasColumnName("Transaction Type")
                    .HasMaxLength(50);

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TOfferEnvelope)
                    .HasForeignKey(d => d.PersonNo)
                    .HasConstraintName("FK_tOffer_Envelope_tPersonal_Information");

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.TOfferEnvelope)
                    .HasForeignKey(d => d.TeamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffer_Envelope_tOffer_Team");

                entity.HasOne(d => d.TOfferingBatches)
                    .WithMany(p => p.TOfferEnvelope)
                    .HasForeignKey(d => new { d.Date, d.Time })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffer_Envelope_tOffering_Batches");
            });

            modelBuilder.Entity<TOfferEnvelopeItems>(entity =>
            {
                entity.HasKey(e => new { e.Date, e.Time, e.EnvelopeId, e.OfferingTypeId });

                entity.ToTable("tOffer_Envelope_Items");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Time).HasMaxLength(8);

                entity.Property(e => e.EnvelopeId)
                    .HasColumnName("EnvelopeID")
                    .HasMaxLength(10);

                entity.Property(e => e.OfferingTypeId).HasColumnName("Offering Type ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(50);

                entity.Property(e => e.LastUpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.OfferingType)
                    .WithMany(p => p.TOfferEnvelopeItems)
                    .HasForeignKey(d => d.OfferingTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffer_Envelope_Items_tOffering_Type");

                entity.HasOne(d => d.TOfferEnvelope)
                    .WithMany(p => p.TOfferEnvelopeItems)
                    .HasForeignKey(d => new { d.Date, d.Time, d.EnvelopeId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffer_Envelope_Items_tOffer_Envelope");
            });

            modelBuilder.Entity<TOfferingBatch>(entity =>
            {
                entity.HasKey(e => new { e.Date, e.Time, e.InputSeq });

                entity.ToTable("tOffering_Batch");

                entity.HasIndex(e => e.EnvelopeId)
                    .HasName("IX_tOffering_Batch");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Time).HasMaxLength(8);

                entity.Property(e => e.InputSeq).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.EnvelopeId)
                    .HasColumnName("EnvelopeID")
                    .HasMaxLength(10);

                entity.Property(e => e.EnvelopeNo)
                    .HasColumnName("Envelope No")
                    .HasMaxLength(10);

                entity.Property(e => e.LastUpdateBy).HasMaxLength(50);

                entity.Property(e => e.LastUpdateTime).HasColumnType("datetime");

                entity.Property(e => e.OfferingName)
                    .HasColumnName("Offering Name")
                    .HasMaxLength(50);

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Remarks).HasMaxLength(50);

                entity.Property(e => e.TeamId)
                    .HasColumnName("TeamID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmt).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.TransactionNo).HasMaxLength(50);

                entity.Property(e => e.TransactionType)
                    .HasColumnName("Transaction Type")
                    .HasMaxLength(50);

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TOfferingBatch)
                    .HasForeignKey(d => d.PersonNo)
                    .HasConstraintName("FK_tOffering_tPersonal_Information");

                entity.HasOne(d => d.TOfferingBatches)
                    .WithMany(p => p.TOfferingBatch)
                    .HasForeignKey(d => new { d.Date, d.Time })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffering_Batch_tOffering_Batches");
            });

            modelBuilder.Entity<TOfferingBatches>(entity =>
            {
                entity.HasKey(e => new { e.Date, e.Time });

                entity.ToTable("tOffering_Batches");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Time).HasMaxLength(8);

                entity.Property(e => e.BatchName).HasMaxLength(20);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(50);

                entity.Property(e => e.LastUpdateTime).HasColumnType("datetime");

                entity.Property(e => e.Posted).HasColumnType("char(1)");
            });

            modelBuilder.Entity<TOfferingItems>(entity =>
            {
                entity.HasKey(e => new { e.Date, e.Time, e.InputSeq, e.OfferingTypeId });

                entity.ToTable("tOffering_Items");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Time).HasMaxLength(8);

                entity.Property(e => e.OfferingTypeId).HasColumnName("Offering Type ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.EnvelopeId)
                    .HasColumnName("EnvelopeID")
                    .HasMaxLength(10);

                entity.Property(e => e.LastUpdateBy).HasMaxLength(50);

                entity.Property(e => e.LastUpdateTime).HasColumnType("datetime");

                entity.HasOne(d => d.OfferingType)
                    .WithMany(p => p.TOfferingItems)
                    .HasForeignKey(d => d.OfferingTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffering_Items_tOffering_Type");

                entity.HasOne(d => d.TOfferingBatch)
                    .WithMany(p => p.TOfferingItems)
                    .HasForeignKey(d => new { d.Date, d.Time, d.InputSeq })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tOffering_Items_tOffering_Batch");
            });

            modelBuilder.Entity<TOfferingType>(entity =>
            {
                entity.HasKey(e => e.OfferingTypeId);

                entity.ToTable("tOffering_Type");

                entity.HasIndex(e => e.OfferingTypeId)
                    .HasName("tOffering_Type$Offering Type ID");

                entity.Property(e => e.OfferingTypeId).HasColumnName("Offering Type ID");

                entity.Property(e => e.OfferingType)
                    .HasColumnName("Offering Type")
                    .HasMaxLength(20);

                entity.Property(e => e.Show).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TOfferTeam>(entity =>
            {
                entity.HasKey(e => e.TeamId);

                entity.ToTable("tOffer_Team");

                entity.Property(e => e.TeamId)
                    .HasColumnName("TeamID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<TPermission>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.TaskId });

                entity.ToTable("tPermission", "ecoa");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.TaskId).HasColumnName("task_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Permitted)
                    .HasColumnName("permitted")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Scope).HasColumnName("scope");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TPermission)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_tPermission_tRole");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TPermission)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tPermission_tTask");
            });

            modelBuilder.Entity<TPersonalFollowUpHistory>(entity =>
            {
                entity.ToTable("tPersonal_Follow_Up_History");

                entity.HasIndex(e => e.Id)
                    .HasName("tPersonal_Follow_Up_History$ID");

                entity.HasIndex(e => e.PersonNo)
                    .HasName("tPersonal_Follow_Up_History$tPersonal_Follow_Up_HistoryPerson No");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Participants).HasMaxLength(50);

                entity.Property(e => e.PersonNo)
                    .IsRequired()
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Reason).HasMaxLength(255);

                entity.Property(e => e.Remarks).HasMaxLength(255);

                entity.Property(e => e.Response).HasMaxLength(30);

                entity.Property(e => e.Time).HasColumnType("time(0)");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TPersonalFollowUpHistory)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tPersonal_Follow_Up_History${77F1C438-8801-4376-994E-417DFB3AA79F}");
            });

            modelBuilder.Entity<TPersonalGiftingAndSkills>(entity =>
            {
                entity.ToTable("tPersonal_Gifting_And_Skills");

                entity.HasIndex(e => e.PersonNo)
                    .HasName("tPersonal_Gifting_And_Skills$tPersonal_Gifting_And_SkillsPerson No");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(20);

                entity.Property(e => e.PersonNo)
                    .IsRequired()
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Remarks).HasMaxLength(255);

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TPersonalGiftingAndSkills)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tPersonal_Gifting_And_Skills${C7BF2889-CA43-4B5D-8B88-5E97B7723219}");
            });

            modelBuilder.Entity<TPersonalInformation>(entity =>
            {
                entity.HasKey(e => e.PersonNo);

                entity.ToTable("tPersonal_Information");

                entity.HasIndex(e => e.EnvelopeNo)
                    .HasName("tPersonal_Information$Envelop ID");

                entity.HasIndex(e => e.IdNo)
                    .HasName("tPersonal_Information$IDCardNo");

                entity.HasIndex(e => e.MemberCategory)
                    .HasName("tPersonal_Information${4B4A8BB3-5CCA-4497-A939-EA909CC855C9}");

                entity.HasIndex(e => e.OfferingName)
                    .HasName("tPersonal_Information$Offering Name");

                entity.HasIndex(e => e.OldMemberId)
                    .HasName("tPersonal_Information$Old Member ID");

                entity.HasIndex(e => e.ParentId)
                    .HasName("tPersonal_Information$Parent ID");

                entity.Property(e => e.PersonNo)
                    .HasColumnName("Person No")
                    .HasMaxLength(10)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.AllowLogin).HasDefaultValueSql("((0))");

                entity.Property(e => e.AttendDate)
                    .HasColumnName("Attend Date")
                    .HasColumnType("date");

                entity.Property(e => e.BaptismDay).HasColumnName("Baptism Day");

                entity.Property(e => e.BaptismMonth).HasColumnName("Baptism Month");

                entity.Property(e => e.BaptismYear).HasColumnName("Baptism Year");

                entity.Property(e => e.BirthDay).HasColumnName("Birth Day");

                entity.Property(e => e.BirthMonth).HasColumnName("Birth Month");

                entity.Property(e => e.BirthYear).HasColumnName("Birth Year");

                entity.Property(e => e.CellLeaderId)
                    .HasColumnName("cell_leader_id")
                    .HasMaxLength(50);

                entity.Property(e => e.ChineseName)
                    .HasColumnName("Chinese Name")
                    .HasMaxLength(30);

                entity.Property(e => e.CombinedOffering)
                    .HasColumnName("Combined Offering")
                    .HasMaxLength(10);

                entity.Property(e => e.Comment).HasMaxLength(1000);

                entity.Property(e => e.CompanyName)
                    .HasColumnName("Company Name")
                    .HasMaxLength(50);

                entity.Property(e => e.ConvictionDate)
                    .HasColumnName("Conviction Date")
                    .HasColumnType("date");

                entity.Property(e => e.ConvictionDay).HasColumnName("Conviction Day");

                entity.Property(e => e.ConvictionMonth).HasColumnName("Conviction Month");

                entity.Property(e => e.ConvictionYear).HasColumnName("Conviction Year");

                entity.Property(e => e.CustomDate1)
                    .HasColumnName("Custom Date 1")
                    .HasColumnType("date");

                entity.Property(e => e.CustomDate2)
                    .HasColumnName("Custom Date 2")
                    .HasColumnType("date");

                entity.Property(e => e.CustomDate3)
                    .HasColumnName("Custom Date 3")
                    .HasColumnType("date");

                entity.Property(e => e.CustomDate4)
                    .HasColumnName("Custom Date 4")
                    .HasColumnType("date");

                entity.Property(e => e.CustomDate5)
                    .HasColumnName("Custom Date 5")
                    .HasColumnType("date");

                entity.Property(e => e.CustomNumber1).HasColumnName("Custom Number 1");

                entity.Property(e => e.CustomNumber2).HasColumnName("Custom Number 2");

                entity.Property(e => e.CustomNumber3).HasColumnName("Custom Number 3");

                entity.Property(e => e.CustomNumber4).HasColumnName("Custom Number 4");

                entity.Property(e => e.CustomNumber5).HasColumnName("Custom Number 5");

                entity.Property(e => e.CustomText1)
                    .HasColumnName("Custom Text 1")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomText2)
                    .HasColumnName("Custom Text 2")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomText3)
                    .HasColumnName("Custom Text 3")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomText4)
                    .HasColumnName("Custom Text 4")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomText5)
                    .HasColumnName("Custom Text 5")
                    .HasMaxLength(50);

                entity.Property(e => e.DeathDate)
                    .HasColumnName("Death Date")
                    .HasColumnType("date");

                entity.Property(e => e.Dialect).HasMaxLength(20);

                entity.Property(e => e.Disability).HasMaxLength(30);

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasColumnName("Display Name")
                    .HasMaxLength(90);

                entity.Property(e => e.District).HasMaxLength(50);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.Education).HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.EmergencyContact)
                    .HasColumnName("Emergency Contact")
                    .HasMaxLength(50);

                entity.Property(e => e.EmergencyPhone)
                    .HasColumnName("Emergency Phone")
                    .HasMaxLength(20);

                entity.Property(e => e.EmergencyRelation)
                    .HasColumnName("Emergency Relation")
                    .HasMaxLength(30);

                entity.Property(e => e.EnglishAddress)
                    .HasColumnName("English Address")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.EnvelopeNo)
                    .HasColumnName("Envelope No")
                    .HasMaxLength(10);

                entity.Property(e => e.FirstName)
                    .HasColumnName("First Name")
                    .HasMaxLength(60);

                entity.Property(e => e.Gender).HasMaxLength(1);

                entity.Property(e => e.HasCell)
                    .HasColumnName("has_cell")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.HomeFax)
                    .HasColumnName("Home Fax")
                    .HasMaxLength(30);

                entity.Property(e => e.HomePhone)
                    .HasColumnName("Home Phone")
                    .HasMaxLength(20);

                entity.Property(e => e.IdNo)
                    .HasColumnName("ID No")
                    .HasMaxLength(20);

                entity.Property(e => e.Immigration).HasMaxLength(50);

                entity.Property(e => e.InputDate)
                    .HasColumnName("Input Date")
                    .HasColumnType("datetime2(0)")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsStaff)
                    .HasColumnName("is_staff")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JoinDate)
                    .HasColumnName("Join Date")
                    .HasColumnType("date");

                entity.Property(e => e.LastName)
                    .HasColumnName("Last Name")
                    .HasMaxLength(50);

                entity.Property(e => e.LastUpdate)
                    .HasColumnName("Last Update")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.LeaveDate)
                    .HasColumnName("Leave Date")
                    .HasColumnType("date");

                entity.Property(e => e.LoginCount).HasDefaultValueSql("((0))");

                entity.Property(e => e.MailAddress).HasMaxLength(500);

                entity.Property(e => e.MailDistrict).HasMaxLength(50);

                entity.Property(e => e.MailRegion).HasMaxLength(50);

                entity.Property(e => e.MaritalStatus)
                    .HasColumnName("Marital Status")
                    .HasMaxLength(20);

                entity.Property(e => e.MarriageDate)
                    .HasColumnName("Marriage Date")
                    .HasColumnType("date");

                entity.Property(e => e.MemberCategory).HasColumnName("Member Category");

                entity.Property(e => e.MemberId).HasColumnName("member_id");

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("Mobile Phone")
                    .HasMaxLength(20);

                entity.Property(e => e.MotherTongue)
                    .HasColumnName("Mother Tongue")
                    .HasMaxLength(20);

                entity.Property(e => e.Nationality).HasMaxLength(20);

                entity.Property(e => e.Occupation).HasMaxLength(30);

                entity.Property(e => e.OccupationType)
                    .HasColumnName("Occupation Type")
                    .HasMaxLength(50);

                entity.Property(e => e.OfferingName)
                    .HasColumnName("Offering Name")
                    .HasMaxLength(50);

                entity.Property(e => e.OldMemberId)
                    .HasColumnName("Old Member ID")
                    .HasMaxLength(20);

                entity.Property(e => e.Origin).HasMaxLength(30);

                entity.Property(e => e.ParentId).HasColumnName("Parent ID");

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Photo).HasColumnType("image");

                entity.Property(e => e.PlaceOfBirth)
                    .HasColumnName("Place of Birth")
                    .HasMaxLength(50);

                entity.Property(e => e.PpsNo)
                    .HasColumnName("PPS No")
                    .HasMaxLength(25);

                entity.Property(e => e.PreviousChurchName)
                    .HasColumnName("Previous Church Name")
                    .HasMaxLength(50);

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");

                entity.Property(e => e.ReceiptName)
                    .HasColumnName("Receipt Name")
                    .HasMaxLength(50);

                entity.Property(e => e.Region).HasMaxLength(50);

                entity.Property(e => e.Role)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Salary).HasMaxLength(20);

                entity.Property(e => e.Salutation).HasMaxLength(20);

                entity.Property(e => e.SameAddress)
                    .HasColumnName("Same Address")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateUser)
                    .HasColumnName("Update User")
                    .HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.WaterBaptismChurch)
                    .HasColumnName("Water Baptism Church")
                    .HasMaxLength(50);

                entity.Property(e => e.WaterBaptismDate)
                    .HasColumnName("Water Baptism Date")
                    .HasColumnType("date");

                entity.Property(e => e.WaterBaptismPastor)
                    .HasColumnName("Water Baptism Pastor")
                    .HasMaxLength(50);

                entity.Property(e => e.WorkFax)
                    .HasColumnName("Work Fax")
                    .HasMaxLength(30);

                entity.Property(e => e.WorkPhone)
                    .HasColumnName("Work Phone")
                    .HasMaxLength(20);

                entity.HasOne(d => d.MemberCategoryNavigation)
                    .WithMany(p => p.TPersonalInformation)
                    .HasForeignKey(d => d.MemberCategory)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tPersonal_Information_tMember_Category");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TPersonalInformation)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_tPersonal_Information_tWebUser1");
            });

            modelBuilder.Entity<TPersonalMinistryHistory>(entity =>
            {
                entity.ToTable("tPersonal_Ministry_History");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChurchName)
                    .HasColumnName("Church Name")
                    .HasMaxLength(50);

                entity.Property(e => e.EndDate).HasColumnName("End Date");

                entity.Property(e => e.MinistryArea)
                    .HasColumnName("Ministry Area")
                    .HasMaxLength(50);

                entity.Property(e => e.PersonNo)
                    .IsRequired()
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Position).HasMaxLength(50);

                entity.Property(e => e.Remarks).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnName("Start Date");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TPersonalMinistryHistory)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tPersonal_Ministry_History_tPersonal_Information");
            });

            modelBuilder.Entity<TPersonalTrainingHistory>(entity =>
            {
                entity.ToTable("tPersonal_Training_History");

                entity.HasIndex(e => e.Id)
                    .HasName("tPersonal_Training_History$ID");

                entity.HasIndex(e => e.PersonNo)
                    .HasName("tPersonal_Training_History$tPersonal_Training_HistoryPerson No");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Category).HasMaxLength(20);

                entity.Property(e => e.Certificate).HasMaxLength(50);

                entity.Property(e => e.EndDate)
                    .HasColumnName("End Date")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.Institute).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.PersonNo)
                    .IsRequired()
                    .HasColumnName("Person No")
                    .HasMaxLength(10);

                entity.Property(e => e.Remarks).HasMaxLength(255);

                entity.Property(e => e.StartDate)
                    .HasColumnName("Start Date")
                    .HasColumnType("datetime2(0)");

                entity.HasOne(d => d.PersonNoNavigation)
                    .WithMany(p => p.TPersonalTrainingHistory)
                    .HasForeignKey(d => d.PersonNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tPersonal_Training_History${B9E1BFD6-4627-42FB-9300-163993EA9951}");
            });

            modelBuilder.Entity<TRole>(entity =>
            {
                entity.ToTable("tRole", "ecoa");

                entity.HasIndex(e => new { e.Name, e.ApplicationId })
                    .HasName("IX_tRole")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ApplicationId).HasColumnName("application_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.ParentRoleId).HasColumnName("parent_role_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.TRole)
                    .HasForeignKey(d => d.ApplicationId)
                    .HasConstraintName("FK_tRole_tApplication");

                entity.HasOne(d => d.ParentRole)
                    .WithMany(p => p.InverseParentRole)
                    .HasForeignKey(d => d.ParentRoleId)
                    .HasConstraintName("FK_tRole_parent_role");
            });

            modelBuilder.Entity<TSalary>(entity =>
            {
                entity.HasKey(e => e.SalaryId);

                entity.ToTable("tSalary");

                entity.HasIndex(e => e.SalaryId)
                    .HasName("tSalary$Salary ID");

                entity.Property(e => e.SalaryId).HasColumnName("Salary ID");

                entity.Property(e => e.SalaryRange)
                    .HasColumnName("Salary Range")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<TTask>(entity =>
            {
                entity.ToTable("tTask", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasColumnName("action")
                    .HasMaxLength(255);

                entity.Property(e => e.ApplicationId).HasColumnName("application_id");

                entity.Property(e => e.Controller)
                    .IsRequired()
                    .HasColumnName("controller")
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DefaultAllow)
                    .HasColumnName("default_allow")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.ParentTaskId).HasColumnName("parent_task_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.TTask)
                    .HasForeignKey(d => d.ApplicationId)
                    .HasConstraintName("FK_tTask_tApplication");

                entity.HasOne(d => d.ParentTask)
                    .WithMany(p => p.InverseParentTask)
                    .HasForeignKey(d => d.ParentTaskId)
                    .HasConstraintName("FK_tTask_parent_task_id");
            });

            modelBuilder.Entity<TTaxinomy>(entity =>
            {
                entity.ToTable("tTaxinomy", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Parent).HasColumnName("parent");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.HasOne(d => d.ParentNavigation)
                    .WithMany(p => p.InverseParentNavigation)
                    .HasForeignKey(d => d.Parent)
                    .HasConstraintName("FK_tTaxinomy_tTaxinomy");
            });

            modelBuilder.Entity<TUser>(entity =>
            {
                entity.ToTable("tUser");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Createdby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LoginCount)
                    .HasColumnName("login_count")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LoginLast)
                    .HasColumnName("login_last")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Updatedby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TUserLog>(entity =>
            {
                entity.ToTable("tUserLog");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Action)
                    .HasColumnName("action")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cell)
                    .HasColumnName("cell")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Input).HasColumnName("input");

                entity.Property(e => e.Ip)
                    .HasColumnName("ip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Page)
                    .HasColumnName("page")
                    .IsUnicode(false);

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TUserLog)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tUserLog_tUser");
            });

            modelBuilder.Entity<TVClient>(entity =>
            {
                entity.HasKey(e => e.ClientId);

                entity.ToTable("tV_Client");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Createdby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender).HasMaxLength(1);

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.Updatedby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TVClientStatus>(entity =>
            {
                entity.HasKey(e => new { e.VisitId, e.ClientId });

                entity.ToTable("tV_ClientStatus");

                entity.Property(e => e.VisitId).HasColumnName("VisitID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Bvisited).HasColumnName("BVisited");

                entity.Property(e => e.Conviction).HasMaxLength(10);

                entity.Property(e => e.Illness).HasMaxLength(50);

                entity.Property(e => e.Relationship).HasMaxLength(20);

                entity.Property(e => e.Remarks).HasMaxLength(100);

                entity.Property(e => e.Status).HasMaxLength(500);

                entity.Property(e => e.Visited).HasColumnType("date");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.TVClientStatus)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_RegClient_tV_Client");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.TVClientStatus)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_RegClient_tV_Registration");
            });

            modelBuilder.Entity<TVLanguage>(entity =>
            {
                entity.HasKey(e => e.LanguageId);

                entity.ToTable("tV_Language");

                entity.Property(e => e.LanguageId).HasColumnName("LanguageID");

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TVPosition>(entity =>
            {
                entity.HasKey(e => e.PositionId);

                entity.ToTable("tV_Position");

                entity.Property(e => e.PositionId).HasColumnName("PositionID");

                entity.Property(e => e.Position).HasMaxLength(10);
            });

            modelBuilder.Entity<TVRegistrant>(entity =>
            {
                entity.HasKey(e => e.RegistrantId);

                entity.ToTable("tV_Registrant");

                entity.Property(e => e.RegistrantId).HasColumnName("RegistrantID");

                entity.Property(e => e.CellLeaderName).HasMaxLength(50);

                entity.Property(e => e.Createdby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender).HasMaxLength(1);

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.Property(e => e.OtherChurch).HasMaxLength(50);

                entity.Property(e => e.PersonNo).HasMaxLength(10);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.Updatedby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TVRegTime>(entity =>
            {
                entity.HasKey(e => new { e.VisitId, e.Date, e.StartTime, e.EndTime });

                entity.ToTable("tV_RegTime");

                entity.Property(e => e.VisitId).HasColumnName("VisitID");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.TVRegTime)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_RegTime_tV_Visit");
            });

            modelBuilder.Entity<TVResponse>(entity =>
            {
                entity.HasKey(e => e.ResponseId);

                entity.ToTable("tV_Response");

                entity.Property(e => e.ResponseId).HasColumnName("ResponseID");

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Appointment).HasColumnType("date");

                entity.Property(e => e.Bappointment)
                    .HasColumnName("BAppointment")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Createdby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Difficulty).HasMaxLength(500);

                entity.Property(e => e.Donation).HasMaxLength(50);

                entity.Property(e => e.FollowedBy).HasMaxLength(50);

                entity.Property(e => e.Need).HasMaxLength(500);

                entity.Property(e => e.Other).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.Question).HasMaxLength(500);

                entity.Property(e => e.Thanksgiving).HasMaxLength(500);

                entity.Property(e => e.Updatedby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VisitId).HasColumnName("VisitID");

                entity.Property(e => e.VisitorId).HasColumnName("VisitorID");

                entity.Property(e => e.Witness).HasMaxLength(500);

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.TVResponse)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_Response_tV_Registration");

                entity.HasOne(d => d.Visitor)
                    .WithMany(p => p.TVResponse)
                    .HasForeignKey(d => d.VisitorId)
                    .HasConstraintName("FK_tV_Response_tV_Visitor");
            });

            modelBuilder.Entity<TVVisit>(entity =>
            {
                entity.HasKey(e => e.VisitId);

                entity.ToTable("tV_Visit");

                entity.Property(e => e.VisitId).HasColumnName("VisitID");

                entity.Property(e => e.Building).HasMaxLength(20);

                entity.Property(e => e.Createdby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.District).HasMaxLength(20);

                entity.Property(e => e.Flat).HasMaxLength(20);

                entity.Property(e => e.Floor).HasMaxLength(20);

                entity.Property(e => e.LanguageId).HasColumnName("LanguageID");

                entity.Property(e => e.LastVisitId).HasColumnName("LastVisitID");

                entity.Property(e => e.OtherLanguage).HasMaxLength(30);

                entity.Property(e => e.Reason).HasMaxLength(500);

                entity.Property(e => e.RegDate).HasColumnType("date");

                entity.Property(e => e.Region).HasMaxLength(20);

                entity.Property(e => e.RegistrantId).HasColumnName("RegistrantID");

                entity.Property(e => e.Remark).HasMaxLength(500);

                entity.Property(e => e.Status).HasMaxLength(10);

                entity.Property(e => e.Street).HasMaxLength(20);

                entity.Property(e => e.Updatedby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Village).HasMaxLength(20);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.TVVisit)
                    .HasForeignKey(d => d.LanguageId)
                    .HasConstraintName("FK_tV_Visit_tV_Language");

                entity.HasOne(d => d.Registrant)
                    .WithMany(p => p.TVVisit)
                    .HasForeignKey(d => d.RegistrantId)
                    .HasConstraintName("FK_tV_Registration_tV_Registrant");
            });

            modelBuilder.Entity<TVVisitor>(entity =>
            {
                entity.HasKey(e => e.VisitorId);

                entity.ToTable("tV_Visitor");

                entity.Property(e => e.VisitorId).HasColumnName("VisitorID");

                entity.Property(e => e.Building).HasMaxLength(20);

                entity.Property(e => e.CellLeaderName).HasMaxLength(50);

                entity.Property(e => e.ChineseName).HasMaxLength(30);

                entity.Property(e => e.Createdby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.District).HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.EnglishName).HasMaxLength(60);

                entity.Property(e => e.Flat).HasMaxLength(20);

                entity.Property(e => e.Floor).HasMaxLength(20);

                entity.Property(e => e.Gender).HasMaxLength(1);

                entity.Property(e => e.HongKongIsland).HasMaxLength(20);

                entity.Property(e => e.Kowloon).HasMaxLength(20);

                entity.Property(e => e.Mgcode)
                    .HasColumnName("MGCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Mgleader).HasColumnName("MGLeader");

                entity.Property(e => e.Ministries).HasMaxLength(50);

                entity.Property(e => e.NewTerritories).HasMaxLength(20);

                entity.Property(e => e.Occupation).HasMaxLength(50);

                entity.Property(e => e.OfficeLocation).HasMaxLength(50);

                entity.Property(e => e.OtherFrequency).HasMaxLength(50);

                entity.Property(e => e.OtherLanguage).HasMaxLength(30);

                entity.Property(e => e.PersonNo).HasMaxLength(10);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.PositionId).HasColumnName("PositionID");

                entity.Property(e => e.ProphetIi).HasColumnName("ProphetII");

                entity.Property(e => e.ProphetIii).HasColumnName("ProphetIII");

                entity.Property(e => e.ProphetIiscore).HasColumnName("ProphetIIScore");

                entity.Property(e => e.Region).HasMaxLength(20);

                entity.Property(e => e.Status).HasMaxLength(10);

                entity.Property(e => e.Street).HasMaxLength(20);

                entity.Property(e => e.Updatedby)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Village).HasMaxLength(20);
            });

            modelBuilder.Entity<TVVisitorLang>(entity =>
            {
                entity.HasKey(e => new { e.VisitorId, e.LanguageId });

                entity.ToTable("tV_VisitorLang");

                entity.Property(e => e.VisitorId).HasColumnName("VisitorID");

                entity.Property(e => e.LanguageId).HasColumnName("LanguageID");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.TVVisitorLang)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_VisitorLang_tV_Language");

                entity.HasOne(d => d.Visitor)
                    .WithMany(p => p.TVVisitorLang)
                    .HasForeignKey(d => d.VisitorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_VisitorLang_tV_Visitor");
            });

            modelBuilder.Entity<TVVisitorTime>(entity =>
            {
                entity.HasKey(e => new { e.VisitorId, e.Date, e.StartTime, e.EndTime });

                entity.ToTable("tV_VisitorTime");

                entity.Property(e => e.VisitorId).HasColumnName("VisitorID");

                entity.HasOne(d => d.Visitor)
                    .WithMany(p => p.TVVisitorTime)
                    .HasForeignKey(d => d.VisitorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_VisitorTime_tV_Visitor");
            });

            modelBuilder.Entity<TVVisitTeam>(entity =>
            {
                entity.HasKey(e => new { e.VisitId, e.VisitorId });

                entity.ToTable("tV_VisitTeam");

                entity.Property(e => e.VisitId).HasColumnName("VisitID");

                entity.Property(e => e.VisitorId).HasColumnName("VisitorID");

                entity.Property(e => e.PositionId).HasColumnName("PositionID");

                entity.HasOne(d => d.Position)
                    .WithMany(p => p.TVVisitTeam)
                    .HasForeignKey(d => d.PositionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_VisitTeam_tV_Position");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.TVVisitTeam)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_VisitTeam_tV_Registration");

                entity.HasOne(d => d.Visitor)
                    .WithMany(p => p.TVVisitTeam)
                    .HasForeignKey(d => d.VisitorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tV_VisitTeam_tV_Visitor");
            });

            modelBuilder.Entity<TWebUser>(entity =>
            {
                entity.ToTable("tWebUser", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activated)
                    .HasColumnName("activated")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastLogin).HasColumnName("last_login");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.SingleAccessToken)
                    .HasColumnName("single_access_token")
                    .HasMaxLength(255);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TWebUserPreference>(entity =>
            {
                entity.ToTable("tWebUserPreference", "ecoa");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedAt).HasColumnName("updated_at");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasMaxLength(255);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TWebUserPreference)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_tWebUserPreference_tWebUser");
            });

            modelBuilder.Entity<TWebUserRole>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.UserId });

                entity.ToTable("tWebUser_Role", "ecoa");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TWebUserRole)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_tWebUser_Role_tRole");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TWebUserRole)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_tWebUser_Role_tWebUser");
            });
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            var currentUser = "web";

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedBy = currentUser;
                    ((BaseEntity)entity.Entity).CreatedTime = DateTime.Now;
                }
                ((BaseEntity)entity.Entity).LastUpdateBy = currentUser;
                ((BaseEntity)entity.Entity).LastUpdateTime = DateTime.Now;
            }
        }
    }
}
