﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TOfferingType
    {
        public TOfferingType()
        {
            TOfferEnvelopeItems = new HashSet<TOfferEnvelopeItems>();
            TOfferingItems = new HashSet<TOfferingItems>();
        }

        public short OfferingTypeId { get; set; }
        public string OfferingType { get; set; }
        public short? Sorting { get; set; }
        public bool? Show { get; set; }

        public ICollection<TOfferEnvelopeItems> TOfferEnvelopeItems { get; set; }
        public ICollection<TOfferingItems> TOfferingItems { get; set; }
    }
}
