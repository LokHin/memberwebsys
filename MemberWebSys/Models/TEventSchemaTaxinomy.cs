﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TEventSchemaTaxinomy
    {
        public int EventSchemaId { get; set; }
        public int TaxinomyId { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public TEventSchema EventSchema { get; set; }
        public TTaxinomy Taxinomy { get; set; }
    }
}
