﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TOccupationType
    {
        public int OccTypeId { get; set; }
        public string OccupationType { get; set; }
    }
}
