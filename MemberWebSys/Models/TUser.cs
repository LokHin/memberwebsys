﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TUser
    {
        public TUser()
        {
            TUserLog = new HashSet<TUserLog>();
        }

        public string Id { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public DateTime? LoginLast { get; set; }
        public int LoginCount { get; set; }
        public string Status { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public string Updatedby { get; set; }
        public DateTime Updatedon { get; set; }

        public ICollection<TUserLog> TUserLog { get; set; }
    }
}
