﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVClient
    {
        public TVClient()
        {
            TVClientStatus = new HashSet<TVClientStatus>();
        }

        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public string Updatedby { get; set; }
        public DateTime Updatedon { get; set; }

        public ICollection<TVClientStatus> TVClientStatus { get; set; }
    }
}
