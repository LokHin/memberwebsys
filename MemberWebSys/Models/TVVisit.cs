﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TVVisit
    {
        public TVVisit()
        {
            TVClientStatus = new HashSet<TVClientStatus>();
            TVRegTime = new HashSet<TVRegTime>();
            TVResponse = new HashSet<TVResponse>();
            TVVisitTeam = new HashSet<TVVisitTeam>();
        }

        public int VisitId { get; set; }
        public int? RegistrantId { get; set; }
        public bool? Accompany { get; set; }
        public bool? Volunteer { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string Village { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
        public string Reason { get; set; }
        public int? LanguageId { get; set; }
        public string OtherLanguage { get; set; }
        public string Remark { get; set; }
        public DateTime? RegDate { get; set; }
        public int? RelationshipRating { get; set; }
        public DateTime? Date { get; set; }
        public int? StartTime { get; set; }
        public int? EndTime { get; set; }
        public string Status { get; set; }
        public string Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public string Updatedby { get; set; }
        public DateTime Updatedon { get; set; }
        public int? LastVisitId { get; set; }

        public TVLanguage Language { get; set; }
        public TVRegistrant Registrant { get; set; }
        public ICollection<TVClientStatus> TVClientStatus { get; set; }
        public ICollection<TVRegTime> TVRegTime { get; set; }
        public ICollection<TVResponse> TVResponse { get; set; }
        public ICollection<TVVisitTeam> TVVisitTeam { get; set; }
    }
}
