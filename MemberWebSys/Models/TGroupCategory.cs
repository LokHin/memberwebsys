﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroupCategory
    {
        public TGroupCategory()
        {
            TGroup = new HashSet<TGroup>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public ICollection<TGroup> TGroup { get; set; }
    }
}
