﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TMeeting
    {
        public TMeeting()
        {
            TMeetingAttendance = new HashSet<TMeetingAttendance>();
        }

        public int MeetingId { get; set; }
        public int GroupNo { get; set; }
        public string Name { get; set; }
        public DateTime? MeetingDate { get; set; }
        public TimeSpan? MeetingTime { get; set; }
        public string Visitors { get; set; }
        public int? AttendTotal { get; set; }
        public string Remarks { get; set; }
        public int? VisitorTotal { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public TGroup GroupNoNavigation { get; set; }
        public ICollection<TMeetingAttendance> TMeetingAttendance { get; set; }
    }
}
