﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TCellMember
    {
        public int MgMemberId { get; set; }
        public int CellId { get; set; }
        public int? Type { get; set; }
        public int? Position { get; set; }
        public DateTime? JoinDate { get; set; }
        public DateTime? LeaveDate { get; set; }
        public string Remarks { get; set; }
        public string AllowedEventCodes { get; set; }

        public TCell Cell { get; set; }
        public TMgMember MgMember { get; set; }
    }
}
