﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TWebUser
    {
        public TWebUser()
        {
            TPersonalInformation = new HashSet<TPersonalInformation>();
            TWebUserPreference = new HashSet<TWebUserPreference>();
            TWebUserRole = new HashSet<TWebUserRole>();
        }

        public int Id { get; set; }
        public string Password { get; set; }
        public bool? Active { get; set; }
        public string SingleAccessToken { get; set; }
        public bool? Activated { get; set; }
        public DateTimeOffset? LastLogin { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }

        public ICollection<TPersonalInformation> TPersonalInformation { get; set; }
        public ICollection<TWebUserPreference> TWebUserPreference { get; set; }
        public ICollection<TWebUserRole> TWebUserRole { get; set; }
    }
}
