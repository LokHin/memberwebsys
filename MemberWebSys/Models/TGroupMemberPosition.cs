﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TGroupMemberPosition
    {
        public TGroupMemberPosition()
        {
            TGroupMemberList = new HashSet<TGroupMemberList>();
        }

        public int GroupMemberPositionId { get; set; }
        public string GroupMemberPosition { get; set; }

        public ICollection<TGroupMemberList> TGroupMemberList { get; set; }
    }
}
