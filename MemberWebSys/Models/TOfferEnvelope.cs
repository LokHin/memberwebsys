﻿using System;
using System.Collections.Generic;

namespace MemberWebSys.Models
{
    public partial class TOfferEnvelope
    {
        public TOfferEnvelope()
        {
            TOfferEnvelopeItems = new HashSet<TOfferEnvelopeItems>();
        }

        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string EnvelopeId { get; set; }
        public string EnvelopeNo { get; set; }
        public string PersonNo { get; set; }
        public string OfferingName { get; set; }
        public string TransactionType { get; set; }
        public string TransactionNo { get; set; }
        public decimal? TotalAmt { get; set; }
        public string TeamId { get; set; }
        public string Remarks { get; set; }
        public string Posted { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }

        public TPersonalInformation PersonNoNavigation { get; set; }
        public TOfferingBatches TOfferingBatches { get; set; }
        public TOfferTeam Team { get; set; }
        public ICollection<TOfferEnvelopeItems> TOfferEnvelopeItems { get; set; }
    }
}
